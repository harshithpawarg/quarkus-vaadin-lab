package com.demo.auditing.service;

import com.demo.auditing.model.CustomRevisionEntity;
import com.demo.auditing.repository.CustomRevisionEntityRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.query.AuditEntity;
import org.hibernate.envers.query.AuditQuery;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.io.File;
import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class AuditManager {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private CustomRevisionEntityRepository customRevisionEntityRepository;


    public static List<Map<String,Object>> entityConfigList = new ArrayList<>();

    private SimpleDateFormat simpleDateTimeFormat = new SimpleDateFormat("MM-dd-yy HH:mm:ss");

    private Set<String> getGetterMethods(Object obj){
        Set<String> fieldsArr = new HashSet<>();
        Method[] methods = obj.getClass().getDeclaredMethods();
        for(Method method : methods){
            if(method.getName().indexOf("get") == 0
                && !(method.getReturnType().getName().contains("java.util.Map"))
                && !(method.getReturnType().getName().contains("java.util.Set"))
                && !(method.getReturnType().getName().contains("java.util.List"))
                ){

                fieldsArr.add(method.getName());
            }
        }
        return fieldsArr;
    };

    @PostConstruct
    private void getEntitiesWithLabel() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        File file = new File("../resources/configuration_files/audit_entity.json");
        entityConfigList = objectMapper.readValue(file, new TypeReference<List<Map<String,Object>>>(){});
    }

    private String getChangesBetweenEntities(String propertyName, Object currentObj, Object prevObj) throws Exception{

        if(prevObj == null)
            prevObj = currentObj.getClass().newInstance();

        String str = "";
        String methodName = "get"+propertyName.substring(0, 1).toUpperCase() + propertyName.substring(1);

        Object result = getValueFromObject(currentObj, methodName);
        Object prevResult = getValueFromObject(prevObj, methodName);

        if(prevResult == null)
            prevResult = "";

        if(result == null)
            result = "";

        if(!result.toString().equals(prevResult.toString()) || methodName.toLowerCase().contains("password"))
            str += propertyName +": "+ prevResult + " --> "+ result + "\n";

        return str;
    }

    private String getEntityDetails(String methodName, Object currentObj) throws Exception{

        String str = "";
        String fieldName = methodName.substring(3);
        Object result = getValueFromObject(currentObj, methodName);

        if(result != null)
            str += fieldName + ": "+ result + "\n";

        return str;
    }

    private Object getValueFromObject(Object obj, String methodName ) throws Exception{
        if(methodName.toLowerCase().contains("password"))
            return "********";

        Method method = obj.getClass().getDeclaredMethod(methodName);
        Object result = method.invoke(obj);

        if(result != null ){
            if(result.getClass().getName().contains("com.demo")){
                Class cls = Class.forName(result.getClass().getName().split("\\$")[0]);
                result = getValueFromObject(result, "getId");
                Object entityObject = entityManager.find(cls, result);
                if(entityObject != null){
                    result = getIdentifier(entityObject);
                }
            }
            if(result.getClass().getSimpleName().equalsIgnoreCase("double")) {
                DecimalFormat df2 = new DecimalFormat("0.00");
                result = df2.format(result);
            }else if(result.getClass().getSimpleName().equalsIgnoreCase("date")){
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                result = sdf.format(result);
            }else if(result.getClass().getSimpleName().equalsIgnoreCase("boolean")){
                result = ((Boolean) result)? "YES" : "NO";
            }
        }
        return result;
    }

    private Object getIdentifier(Object entity) throws Exception {
        String entityName = entity.getClass().getSimpleName();
        String keyMethod = "getId";

        List<Map<String,Object>> entityConfig = entityConfigList.stream().filter(config ->
            config.get("entityName").toString().equals(entityName)
        ).collect(Collectors.toList());

        if(entityConfig.size() > 0){
            String [] methods = entityConfig.get(0).get("method").toString().split("\\.");
            if(methods.length == 1) keyMethod = methods[0];
        }

        return getValueFromObject(entity, keyMethod);
    }

    public JSONObject getAuditDetails(Object[] obj, Class cls, CustomRevisionEntity customRevisionEntity, AuditReader reader) throws Exception{

        JSONObject auditDetails = new JSONObject();
        String action = obj[2].toString();
        Object entity = obj[0];
        String modifiedDetails = "";
        Class entityClass = entity.getClass();
        Method method = entityClass.getDeclaredMethod("getId");
        Object entityId = method.invoke(entity);

        AuditQuery previousRecord = reader.createQuery()
            .forRevisionsOfEntity(cls, false, true)
            .add(AuditEntity.property("id").eq(entityId))
            .add(AuditEntity.revisionNumber().lt(customRevisionEntity.getId()))
            .addOrder(AuditEntity.revisionNumber().desc())
            .setMaxResults(1);

        List prevObjs = previousRecord.getResultList();
        Object previousEntity = null;
        if (prevObjs.size() > 0) {
            Object[] prevObj = (Object[]) prevObjs.get(0);
            previousEntity = prevObj[0];
        }

        if (action.equalsIgnoreCase("add")) {
            Set<String> methods = getGetterMethods(entity);
            for(String getterMethod : methods){
                modifiedDetails += getEntityDetails(getterMethod, entity);
            }
        } else if(action.equalsIgnoreCase("del") && previousEntity != null){
            Set<String> methods = getGetterMethods(entity);
            for(String getterMethod : methods){
                modifiedDetails += getEntityDetails(getterMethod, previousEntity);
            }
        } else{
            Set<String> propertiesChanged = (Set<String>) obj[3];
            for (String propertyName : propertiesChanged) {
                modifiedDetails += getChangesBetweenEntities(propertyName, entity, previousEntity);
            }
        }

        if (action.equalsIgnoreCase("mod"))
            action = "Modified";
        else if (action.equalsIgnoreCase("add"))
            action = "Created";
        else if (action.equalsIgnoreCase("del"))
            action = "Deleted";

        auditDetails.put("action", action);
        auditDetails.put("revId", customRevisionEntity.getId());
        auditDetails.put("modifiedBy", customRevisionEntity.getUsername());
        auditDetails.put("key", customRevisionEntity.getKey());
        auditDetails.put("modifiedAt", simpleDateTimeFormat.format(customRevisionEntity.getRevisionDate()));
        auditDetails.put("modifiedDetails", modifiedDetails);
        auditDetails.put("entityName", cls.getSimpleName());
        auditDetails.put("packageName", cls.getPackage().getName());
        return auditDetails;
    }

    public Map<String, Object> getRevisionDetails(Object[] obj) throws Exception {
        Map<String, Object> result = new HashMap<>();

        Set<String> methods = getGetterMethods(obj[0]);
        for(String method : methods){
            Object output = getValueFromObject(obj[0], method);
            result.put(method.substring(3), output);
        }
        CustomRevisionEntity customRevisionEntity = (CustomRevisionEntity) obj[1];
        String action = obj[2].toString();
        if (action.equalsIgnoreCase("mod"))
            action = "Modified";
        else if (action.equalsIgnoreCase("add"))
            action = "Created";
        else if (action.equalsIgnoreCase("del"))
            action = "Deleted";

        result.put("action", action);
        result.put("modifiedBy", customRevisionEntity.getUsername());
        result.put("modifiedAt", simpleDateTimeFormat.format(customRevisionEntity.getRevisionDate()));
        return result;
    }

    public List<CustomRevisionEntity> findAll(){
        return customRevisionEntityRepository.findAll(Sort.by(Sort.Direction.DESC, "id"));
    }

    public CustomRevisionEntity findById(int revisionId){
        return customRevisionEntityRepository.findById(revisionId);
    }

    @Transactional
    public List<Map<String, Object>> getRevisionHistory(int revNumber, String entityClass) throws Exception{
        List<Map<String, Object>> revisionList = new ArrayList<>();
        System.out.println("entity        "+entityManager.isOpen());
        AuditReader reader = AuditReaderFactory.get(entityManager);
        Class cls = Class.forName(entityClass);
        AuditQuery currentRevqQuery = reader.createQuery()
                .forRevisionsOfEntity(cls, false,true)
                .add(AuditEntity.revisionNumber().eq(Integer.valueOf(revNumber)));

        List<Object []> currRevObj = currentRevqQuery.getResultList();
        for(Object[] currObj : currRevObj) {
            Class entity = currObj[0].getClass();
            Method method = entity.getDeclaredMethod("getId");
            Object entityId = method.invoke(currObj[0], null);
            AuditQuery query = reader.createQuery()
                    .forRevisionsOfEntity(cls, false, true)
                    .add(AuditEntity.property("id").eq(entityId))
                    .add(AuditEntity.revisionNumber().lt(Integer.valueOf(revNumber + 1)))
                    .addOrder(AuditEntity.revisionNumber().desc());
            List<Object[]> objs = query.getResultList();

            for (Object[] obj : objs) {
                revisionList.add(getRevisionDetails(obj));
            }
        }
        return revisionList;
    }
}
