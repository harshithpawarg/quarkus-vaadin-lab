package com.demo.auditing.model;

import com.demo.auditing.listener.CustomRevisionListener;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.DefaultRevisionEntity;
import org.hibernate.envers.ModifiedEntityNames;
import org.hibernate.envers.RevisionEntity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@RevisionEntity(CustomRevisionListener.class)
@Table(name="custom_revision_entities")
public class CustomRevisionEntity extends DefaultRevisionEntity {
    private String username;

    private String key;

    private String revtype;

    @ElementCollection
    @JoinTable(
        name = "REVCHANGES",
        joinColumns = @JoinColumn( name = "REV" )
    )
    @Column( name = "ENTITYNAME" )
    @ModifiedEntityNames
    private Set<String> modifiedEntityNames = new HashSet<>();

}
