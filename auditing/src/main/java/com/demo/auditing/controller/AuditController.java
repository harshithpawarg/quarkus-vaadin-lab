package com.demo.auditing.controller;

import com.demo.auditing.model.CustomRevisionEntity;
import com.demo.auditing.repository.CustomRevisionEntityRepository;
import com.demo.auditing.service.AuditManager;
import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.query.AuditEntity;
import org.hibernate.envers.query.AuditQuery;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping("/api")
public class AuditController {

    @PersistenceContext
    private EntityManager entityManager;

//    @Autowired
//    private DealManager dealManager;

    @Autowired
    private CustomRevisionEntityRepository customRevisionEntityRepository;

    @Autowired
    private AuditManager auditManager;

    @PostMapping("/getEntityList")
    public List<Map<String,Object>> getEntityList() throws Exception {
        return AuditManager.entityConfigList;
    }

    @PostMapping("/getAllAudits")
    public String getAllAudits(@RequestBody Map<String, Object> body) throws Exception {
        JSONArray auditList = new JSONArray();
        System.out.println("REQUEST: getAllAudits, BODY: " + body);
        List<String> entityList = new ArrayList<>();
        String entityName = body.get("entityName").toString();
        String packageName = "com.demo";
        if(body.get("package") != null)
            packageName = body.get("package").toString();
        if(entityName.equalsIgnoreCase("all")){
            List<Map<String,Object>> entities = AuditManager.entityConfigList;
            for(Map<String,Object> entity : entities) {
                entityList.add(entity.get("package") +"." + entity.get("entityName"));
            }
        }else{
            entityList.add(packageName +"."+entityName);
        }

        AuditReader reader = AuditReaderFactory.get(entityManager);

        Integer pageNo = Integer.parseInt(body.get("offset").toString());
        Integer pageSize = Integer.parseInt(body.get("pageSize").toString());
        List<Map<String, Object>> sortedField = (List<Map<String, Object>>)body.get("sorted");
        List<Map<String, Object>> filteredField = (List<Map<String, Object>>)body.get("filtered");
        String actionFilter = body.get("action").toString();

        String revisionType = "";
        if (actionFilter.equalsIgnoreCase("Modified"))
            revisionType = "MOD";
        else if (actionFilter.equalsIgnoreCase("Created"))
            revisionType = "ADD";
        else if (actionFilter.equalsIgnoreCase("Deleted"))
            revisionType = "DEL";

        Long totalCount = 0L;//customRevisionEntityRepository.countByModifiedEntityNamesIn(entityList);

        Sort sort = null;
        List<Sort.Order> orders = new ArrayList<>();
        List<CustomRevisionEntity> customRevisionEntities = null;

        if(sortedField.size() > 0){
            Map<String, Object> sortObj = sortedField.get(0);
            Boolean desc = (Boolean) sortObj.get("desc");
            String prop = (String) sortObj.get("id");
            if(prop.equalsIgnoreCase("revId")) prop = "id";
            else if (prop.equalsIgnoreCase("modifiedBy")) prop = "username";
            else if (prop.equalsIgnoreCase("entityName")) prop = "modifiedEntityNames";
            else if (prop.equalsIgnoreCase("modifiedAt")) prop = "timestamp";
            if(desc) {
                orders.add(new Sort.Order(Sort.Direction.DESC, prop));
            }
            else
                orders.add(new Sort.Order(Sort.Direction.ASC, prop));
        }else {
            orders.add(new Sort.Order(Sort.Direction.DESC, "timestamp"));
        }

        Pageable pageable = PageRequest.of(pageNo, pageSize, Sort.by(orders));

        Boolean isModifiedByOrKey = false;

        for (Map<String,Object> filter : filteredField) {
            if (filter.get("id").toString().equalsIgnoreCase("modifiedBy"))
                isModifiedByOrKey = true;
            if (filter.get("id").toString().equalsIgnoreCase("key"))
                isModifiedByOrKey = true;
        }


        if(filteredField.size() > 0 && isModifiedByOrKey) {
            String username = "", key = "";
            for(Map<String, Object> filter : filteredField) {
                if(filter.get("id").toString().equalsIgnoreCase("modifiedBy")) {
                    username = (String) filter.get("value");
                    if(username.length() <=3 )
                        username = "";
                }else if(filter.get("id").toString().equalsIgnoreCase("key")) {
                    key = (String) filter.get("value");
                    if(key.length() <=3 )
                        key = "";
                }
            }
            customRevisionEntities = customRevisionEntityRepository.findAll();
            //TODO: As this method is throwing error i am commenting.
            //customRevisionEntities = customRevisionEntityRepository.findDistinctByUsernameIgnoreCaseContainingAndKeyIgnoreCaseContainingAndRevtypeIgnoreCaseContaining(username, key, revisionType, pageable);
        }else {
            customRevisionEntities = customRevisionEntityRepository.findAll();
//            customRevisionEntities = customRevisionEntityRepository.findAllDistinctByModifiedEntityNamesInAndRevtypeIgnoreCaseContaining(entityList,revisionType, pageable);
        }

        for(CustomRevisionEntity customRevisionEntity : customRevisionEntities) {
            Set<String> entityNames = customRevisionEntity.getModifiedEntityNames();

            for (String entityClass : entityNames) {
                Class cls = Class.forName(entityClass);
                AuditQuery query = reader.createQuery()
                    .forRevisionsOfEntityWithChanges(cls, true)
                    .add(AuditEntity.revisionNumber().eq(Integer.valueOf(customRevisionEntity.getId())));

                List<Object[]> objs = query.getResultList();

                if (objs.size() == 0)
                    continue;

                Object[] obj = objs.get(0);

                JSONObject auditDetails = auditManager.getAuditDetails(obj, cls, customRevisionEntity, reader);
                auditList.put(auditDetails);
            }
        }

        return new JSONObject().put("auditList", auditList).put("count", totalCount).toString();
    }

    @PostMapping("/getRevisionsOfEntity")
    public String getRevisionsOfEntity(@RequestBody Map<String, Object> body) throws Exception {
        JSONArray revisionList = new JSONArray();
        System.out.println("ddddddddddddddddddd" + body);
        AuditReader reader = AuditReaderFactory.get(entityManager);
        int revNumber = Integer.parseInt(body.get("revNumber").toString());

        String entityClass = body.get("entityName").toString();
        Class cls = Class.forName(entityClass);
        AuditQuery currentRevqQuery = reader.createQuery()
            .forRevisionsOfEntity(cls, false,true)
            .add(AuditEntity.revisionNumber().eq(Integer.valueOf(revNumber)));

        List<Object []> currRevObj = currentRevqQuery.getResultList();
        for(Object[] currObj : currRevObj) {
            Class entity = currObj[0].getClass();
            Method method = entity.getDeclaredMethod("getId");
            Object entityId = method.invoke(currObj[0], null);
            AuditQuery query = reader.createQuery()
                .forRevisionsOfEntity(cls, false, true)
                .add(AuditEntity.property("id").eq(entityId))
                .add(AuditEntity.revisionNumber().lt(Integer.valueOf(revNumber + 1)))
                .addOrder(AuditEntity.revisionNumber().desc());
            List<Object[]> objs = query.getResultList();

            for (Object[] obj : objs) {
                revisionList.put(auditManager.getRevisionDetails(obj));
            }
        }
        return revisionList.toString();
    }

}
