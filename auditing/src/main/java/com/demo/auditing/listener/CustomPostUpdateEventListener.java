package com.demo.auditing.listener;

import com.demo.auditing.configuration.ContextLookup;
import com.demo.common.service.AuditContext;
import org.hibernate.envers.boot.internal.EnversService;
import org.hibernate.envers.event.spi.EnversPostUpdateEventListenerImpl;
import org.hibernate.event.spi.PostUpdateEvent;

public class CustomPostUpdateEventListener extends EnversPostUpdateEventListenerImpl {

    public CustomPostUpdateEventListener(EnversService enversService) {
        super(enversService);
    }

    @Override
    public void onPostUpdate(PostUpdateEvent event) {
        if (AuditContext.getAuditMode()) {
            ContextLookup.setCurrentEntity(event.getEntity());
            super.onPostUpdate(event);
            System.out.println("On Post Update event ==>>>> "+event.getEntity().getClass().getSimpleName());
        }
    }
}
