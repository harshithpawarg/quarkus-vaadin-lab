package com.demo.auditing.listener;

import com.demo.common.service.AuditContext;
import com.demo.auditing.configuration.ContextLookup;
import org.hibernate.envers.boot.internal.EnversService;
import org.hibernate.envers.event.spi.EnversPostInsertEventListenerImpl;
import org.hibernate.event.spi.PostInsertEvent;

public class CustomPostInsertEventListener extends EnversPostInsertEventListenerImpl {

    public CustomPostInsertEventListener(EnversService enversService) {
        super(enversService);
    }
    
    @Override
    public void onPostInsert(PostInsertEvent event) {

        if (AuditContext.getAuditMode()) {
            ContextLookup.setCurrentEntity(event.getEntity());
            super.onPostInsert(event);
            System.out.println("On Post Insert event ==>>>> "+event.getEntity().getClass().getSimpleName());
        }

    }
}
