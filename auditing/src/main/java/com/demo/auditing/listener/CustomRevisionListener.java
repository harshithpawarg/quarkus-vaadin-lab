package com.demo.auditing.listener;

import com.demo.auditing.configuration.ContextLookup;
import com.demo.auditing.model.CustomRevisionEntity;
import com.demo.auditing.service.AuditManager;
import com.demo.common.service.AuditContext;
import com.demo.common.service.KeycloakService;
import org.hibernate.envers.EntityTrackingRevisionListener;
import org.hibernate.envers.RevisionType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

@Service
public class CustomRevisionListener implements EntityTrackingRevisionListener {

    @Autowired
    private KeycloakService keycloakService;

    @Override
    public void entityChanged(Class entityClass,
        String entityName,
        Serializable entityId,
        RevisionType revisionType,
        Object revisionEntity ) {

        if (!AuditContext.getAuditMode())
            return;

//        AutoWireHelper.autowire(this, this.keycloakService);
        CustomRevisionEntity customRevisionEntity = (CustomRevisionEntity) revisionEntity;
        Object obj = ContextLookup.getCurrentEntity();

        if (keycloakService != null && keycloakService.getCurrentUserEmail() != null) {
            customRevisionEntity.setUsername(keycloakService.getCurrentUserEmail());
        } else {
            customRevisionEntity.setUsername("Influx Application");
        }

        System.out.println("--------------------------------------------------");
        System.out.println("MODIFIED ENTITY: "+ entityName);
        System.out.println("MODIFIED ENTITY ID: "+ entityId);
        System.out.println("MODIFIED ENTITY TYPE: "+ revisionType.name());
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");

        try {
            List<Map<String,Object>> entities = AuditManager.entityConfigList;
            for(Map<String, Object> entity : entities) {
                if(obj.getClass().getSimpleName().equalsIgnoreCase(entity.get("entityName").toString())){
                    String [] methods = entity.get("method").toString().split("\\.");
                    if(methods.length > 1){
                        Object object = getValueFromObject(obj, methods[0]);
                        if(object != null)
                            customRevisionEntity.setKey((String) getValueFromObject(object, methods[1]));
                        else
                            System.out.println(obj.getClass().getName()+"."+methods[0]+" returned null");
                    }else{
                        customRevisionEntity.setKey((String) getValueFromObject(obj, methods[0]));
                    }
                    break;
                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        customRevisionEntity.setRevtype(revisionType.name());
    }

    @Override
    public void newRevision( Object revisionEntity ) {

    }

    private Object getValueFromObject(Object obj, String methodName ) throws Exception{
        Method method = obj.getClass().getDeclaredMethod(methodName);
        Object result = method.invoke(obj, null);

        return result;
    }

}
