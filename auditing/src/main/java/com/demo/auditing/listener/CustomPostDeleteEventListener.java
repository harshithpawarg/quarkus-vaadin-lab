package com.demo.auditing.listener;

import com.demo.common.service.AuditContext;
import com.demo.auditing.configuration.ContextLookup;
import org.hibernate.envers.boot.internal.EnversService;
import org.hibernate.envers.event.spi.EnversPostDeleteEventListenerImpl;
import org.hibernate.event.spi.PostDeleteEvent;

public class CustomPostDeleteEventListener extends EnversPostDeleteEventListenerImpl {

    public CustomPostDeleteEventListener(EnversService enversService) {
        super(enversService);
    }
    
    @Override
    public void onPostDelete(PostDeleteEvent event) {
        if (AuditContext.getAuditMode()) {
            ContextLookup.setCurrentEntity(event.getEntity());
            super.onPostDelete(event);
            System.out.println("On Post Delete event ==>>>> "+event.getEntity().getClass().getSimpleName());
        }
    }
}
