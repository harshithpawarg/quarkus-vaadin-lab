package com.demo.auditing.configuration;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
//import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class ContextLookup {
    private static ApplicationContext sApplicationContext;

    public static Object getCurrentEntity() {
        return currentEntity;
    }

    public static void setCurrentEntity(Object currentEntity) {
        ContextLookup.currentEntity = currentEntity;
    }

    private static Object currentEntity;

//    @Override
//    public void setApplicationContext( ApplicationContext aApplicationContext )
//        throws BeansException {
//        setContext( aApplicationContext );
//    }

    public static void setContext( ApplicationContext aApplicationContext ) {
        sApplicationContext = aApplicationContext;
    }

}
