package com.demo.auditing.configuration;

import com.demo.auditing.listener.CustomPostDeleteEventListener;
import com.demo.auditing.listener.CustomPostInsertEventListener;
import com.demo.auditing.listener.CustomPostUpdateEventListener;
import org.hibernate.envers.boot.internal.EnversService;
import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.EventType;
import org.hibernate.internal.SessionFactoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManagerFactory;

@Component
public class EnversListenerConfiguration {

    @Autowired
    EntityManagerFactory entityManagerFactory;

    @PostConstruct
    protected void init() {
        SessionFactoryImpl sessionFactory = entityManagerFactory.unwrap(SessionFactoryImpl.class);
        EventListenerRegistry registry = sessionFactory.getServiceRegistry().getService(EventListenerRegistry.class);
        EnversService enversService = sessionFactory.getServiceRegistry().getService(EnversService.class);

        registry.getEventListenerGroup(EventType.POST_INSERT).appendListener(new CustomPostInsertEventListener(enversService));
        registry.getEventListenerGroup(EventType.POST_UPDATE).appendListener(new CustomPostUpdateEventListener(enversService));
        registry.getEventListenerGroup(EventType.POST_DELETE).appendListener(new CustomPostDeleteEventListener(enversService));
    }
}
