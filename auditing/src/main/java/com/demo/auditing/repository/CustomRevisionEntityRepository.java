package com.demo.auditing.repository;

import com.demo.auditing.model.CustomRevisionEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomRevisionEntityRepository extends JpaRepository<CustomRevisionEntity, Long> {
//    List<CustomRevisionEntity> findAllByModifiedEntityNamesInAndRevtypeIgnoreCaseContaining(List<String> entities, String revtype, Pageable pageable);
//
//    Long countByModifiedEntityNamesIn(List<String> entities);
//
//    List<CustomRevisionEntity> findAllByUsernameIgnoreCaseContainingAndKeyIgnoreCaseContainingAndRevtypeIgnoreCaseContaining(String username, String key, String revtype, Pageable pageable);

    CustomRevisionEntity findById(int aLong);
}
