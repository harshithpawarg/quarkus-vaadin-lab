package com.demo.editors;

import com.demo.common.model.Party;
import com.demo.common.notifications.Broadcaster;
import com.demo.common.repository.PartyRepository;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.KeyNotifier;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
//import com.vaadin.flow.spring.annotation.SpringComponent;
//import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * A simple example to introduce building forms. As your real application is probably much
 * more complicated than this example, you could re-use this form in multiple places. This
 * example component is only used in MainView.
 * <p>
 * In a real world application you'll most likely using a common super class for all your
 * forms - less code, better UX.
 */
//@SpringComponent
//@UIScope
public class PartyEditor extends Dialog implements KeyNotifier {

    private final PartyRepository repository;

    /**
     * The currently edited party
     */
    private Party party;
    /* Fields to edit properties in Party entity */
    TextField name = new TextField("Name");
    EmailField email = new EmailField("Email");
    Checkbox isFunder = new Checkbox("Is Funder");
    TextField businessLegalName = new TextField("Business Legal Name");

    /* Action buttons */
    Button save = new Button("Save", VaadinIcon.CHECK.create());
    Button cancel = new Button("Cancel");
    Button delete = new Button("Delete", VaadinIcon.TRASH.create());
    HorizontalLayout actions = new HorizontalLayout(save, cancel, delete);

    Binder<Party> binder = new Binder<>(Party.class);
    private ChangeHandler changeHandler;

    @Autowired
    public PartyEditor(PartyRepository repository) {
        this.repository = repository;
        HorizontalLayout inputFields = new HorizontalLayout(name, email, businessLegalName);
        VerticalLayout layout = new VerticalLayout();
        layout.add(inputFields, isFunder, actions);
//        dialog = new Dialog(layout);

        addDialogCloseActionListener(e -> {
            close();
        });
        add(layout);
        // bind using naming convention
        binder.bindInstanceFields(this);

        // Configure and style components
//        setSpacing(true);

        save.getElement().getThemeList().add("primary");
        delete.getElement().getThemeList().add("error");

        addKeyPressListener(Key.ENTER, e -> save());

        // wire action buttons to save, delete and reset
        save.addClickListener(e -> save());
        delete.addClickListener(e -> delete());
        cancel.addClickListener(e -> {
            editParty(party);
            close();
        });
        close();
    }

    void delete() {
        repository.delete(party);
        Broadcaster.broadcast(repository.findAll());
        changeHandler.onChange();
        close();
    }

    void save() {
        repository.save(party);
        Broadcaster.broadcast(repository.findAll());
        changeHandler.onChange();
        close();
    }

    public interface ChangeHandler {
        void onChange();
    }

    public final void editParty(Party c) {
        if (c == null) {
            close();
            return;
        }
        final boolean persisted = c.getId() != null;
        if (persisted) {
            // Find fresh entity for editing
            party = repository.findById(c.getId()).get();
        }
        else {
            party = c;
        }
        cancel.setVisible(persisted);

        // Bind party properties to similarly named fields
        // Could also use annotation or "manual binding" or programmatically
        // moving values from fields to entities before saving
        binder.setBean(party);

        open();
        // Focus first name initially
        name.focus();
    }

    public void setChangeHandler(ChangeHandler h) {
        // ChangeHandler is notified when either save or delete
        // is clicked
        changeHandler = h;
    }

}
