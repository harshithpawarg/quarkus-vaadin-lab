package com.demo.config;

import com.demo.common.service.MqttManager;
import com.demo.common.service.PartyManager;
import io.quarkus.runtime.ShutdownEvent;
import io.quarkus.runtime.StartupEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

@ApplicationScoped
public class AppLifecycleBean {

    private static final Logger LOGGER = LoggerFactory.getLogger("ListenerBean");


    @Inject
    private MqttManager mqttManager;

    @Inject
    private PartyManager partyManager;

    void onStart(@Observes StartupEvent ev) {
        LOGGER.info("The application is starting...{}");
        mqttManager.init();
        partyManager.setOnMessageReceived();

    }

    void onStop(@Observes ShutdownEvent ev) {
        LOGGER.info("The application is stopping... {}");
    }
}
