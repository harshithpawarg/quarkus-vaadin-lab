package com.demo;

import javax.enterprise.context.RequestScoped;

@RequestScoped
public class RequestBean {

  public String getText() {
    return "Hello from RequestBean";
  }
}
