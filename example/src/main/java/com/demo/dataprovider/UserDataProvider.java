package com.demo.dataprovider;

import com.demo.common.model.User;
import com.demo.common.repository.UserRepository;
import com.vaadin.flow.component.crud.CrudFilter;
import com.vaadin.flow.data.provider.AbstractBackEndDataProvider;
import com.vaadin.flow.data.provider.Query;
import com.vaadin.flow.data.provider.SortDirection;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

@Component
public class UserDataProvider extends AbstractBackEndDataProvider<User, CrudFilter> {

    private UserRepository userRepository;
    // A real app should hook up something like JPA
    List<User> DATABASE;

    private Consumer<Long> sizeChangeListener;

    private int pageSize = 5;
    private int pageNumber = 0;

    public UserDataProvider(UserRepository userRepository){
        this.userRepository = userRepository;
        this.DATABASE = getAllUsers();
    }

    private List<User> getAllUsers(){
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        return userRepository.findAll(pageable).getContent();

    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void refresh(){
        this.DATABASE = getAllUsers();
    }

    public int getPageRecordCount(){
        return getAllUsers().size();
    }
    
    @Override
    protected Stream<User> fetchFromBackEnd(Query<User, CrudFilter> query) {
        int offset = query.getOffset();
        int limit = query.getLimit();

        Stream<User> stream = DATABASE.stream();

        if (query.getFilter().isPresent()) {
            stream = stream
                    .filter(predicate(query.getFilter().get()))
                    .sorted(comparator(query.getFilter().get()));
        }

        return stream.skip(offset).limit(limit);
    }

    @Override
    protected int sizeInBackEnd(Query<User, CrudFilter> query) {
        // For RDBMS just execute a SELECT COUNT(*) ... WHERE query
        long count = fetchFromBackEnd(query).count();

        if (sizeChangeListener != null) {
            sizeChangeListener.accept(count);
        }

        return (int) count;
    }

    void setSizeChangeListener(Consumer<Long> listener) {
        sizeChangeListener = listener;
    }

    private Predicate<User> predicate(CrudFilter filter) {
        // For RDBMS just generate a WHERE clause
        return filter.getConstraints().entrySet().stream()
                .map(constraint -> (Predicate<User>) user -> {
                    try {
                        Object value = valueOf(constraint.getKey(), user);
                        return value != null && value.toString().toLowerCase()
                                .contains(constraint.getValue().toLowerCase());
                    } catch (Exception e) {
                        e.printStackTrace();
                        return false;
                    }
                })
                .reduce(Predicate::and)
                .orElse(e -> true);
    }

    private  Comparator<User> comparator(CrudFilter filter) {
        // For RDBMS just generate an ORDER BY clause
        return filter.getSortOrders().entrySet().stream()
                .map(sortClause -> {
                    try {
                        Comparator<User> comparator
                                = Comparator.comparing(user ->
                                (Comparable) valueOf(sortClause.getKey(), user));

                        if (sortClause.getValue() == SortDirection.DESCENDING) {
                            comparator = comparator.reversed();
                        }

                        return comparator;
                    } catch (Exception ex) {
                        return (Comparator<User>) (o1, o2) -> 0;
                    }
                })
                .reduce(Comparator::thenComparing)
                .orElse((o1, o2) -> 0);
    }

    private  Object valueOf(String fieldName, User user) {
        try {
            Field field = User.class.getDeclaredField(fieldName);
            field.setAccessible(true);
            return field.get(user);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public void persist(User item) {
        userRepository.save(item);
        this.DATABASE = getAllUsers();
    }

    public void delete(User item) {
        userRepository.delete(item);
        this.DATABASE = getAllUsers();
    }
}