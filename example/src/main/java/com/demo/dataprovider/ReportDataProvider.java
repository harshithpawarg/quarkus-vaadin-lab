package com.demo.dataprovider;

import com.demo.common.model.Report;
import com.demo.common.service.ReportManager;
import com.vaadin.flow.component.crud.CrudFilter;
import com.vaadin.flow.data.provider.AbstractBackEndDataProvider;
import com.vaadin.flow.data.provider.Query;
import com.vaadin.flow.data.provider.SortDirection;
import org.springframework.stereotype.Component;
//import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Field;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

@Component
public class ReportDataProvider extends AbstractBackEndDataProvider<Report, CrudFilter> {

    private ReportManager reportManager;
    // A real app should hook up something like JPA
    List<Report> DATABASE;

    private Consumer<Long> sizeChangeListener;

    public ReportDataProvider(ReportManager reportManager){
        this.reportManager = reportManager;
        this.DATABASE = reportManager.findAll();
    }

    @Override
    protected Stream<Report> fetchFromBackEnd(Query<Report, CrudFilter> query) {
        int offset = query.getOffset();
        int limit = query.getLimit();

        Stream<Report> stream = DATABASE.stream();

        if (query.getFilter().isPresent()) {
            stream = stream
                    .filter(predicate(query.getFilter().get()))
                    .sorted(comparator(query.getFilter().get()));
        }

        return stream.skip(offset).limit(limit);
    }

    @Override
    protected int sizeInBackEnd(Query<Report, CrudFilter> query) {
        // For RDBMS just execute a SELECT COUNT(*) ... WHERE query
        long count = fetchFromBackEnd(query).count();

        if (sizeChangeListener != null) {
            sizeChangeListener.accept(count);
        }

        return (int) count;
    }

    void setSizeChangeListener(Consumer<Long> listener) {
        sizeChangeListener = listener;
    }

    private Predicate<Report> predicate(CrudFilter filter) {
        // For RDBMS just generate a WHERE clause
        return filter.getConstraints().entrySet().stream()
                .map(constraint -> (Predicate<Report>) report -> {
                    try {
                        Object value = valueOf(constraint.getKey(), report);
                        return value != null && value.toString().toLowerCase()
                                .contains(constraint.getValue().toLowerCase());
                    } catch (Exception e) {
                        e.printStackTrace();
                        return false;
                    }
                })
                .reduce(Predicate::and)
                .orElse(e -> true);
    }

    private  Comparator<Report> comparator(CrudFilter filter) {
        // For RDBMS just generate an ORDER BY clause
        return filter.getSortOrders().entrySet().stream()
                .map(sortClause -> {
                    try {
                        Comparator<Report> comparator
                                = Comparator.comparing(report ->
                                (Comparable) valueOf(sortClause.getKey(), report));

                        if (sortClause.getValue() == SortDirection.DESCENDING) {
                            comparator = comparator.reversed();
                        }

                        return comparator;
                    } catch (Exception ex) {
                        return (Comparator<Report>) (o1, o2) -> 0;
                    }
                })
                .reduce(Comparator::thenComparing)
                .orElse((o1, o2) -> 0);
    }

    private  Object valueOf(String fieldName, Report report) {
        try {
            Field field = Report.class.getDeclaredField(fieldName);
            field.setAccessible(true);
            return field.get(report);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

//    @Transactional
    public void persist(Report report) {
        reportManager.persist(report);
        this.DATABASE = reportManager.findAll();
    }

    public void delete(Report item) {
        reportManager.delete(item);
        this.DATABASE = reportManager.findAll();
    }
}