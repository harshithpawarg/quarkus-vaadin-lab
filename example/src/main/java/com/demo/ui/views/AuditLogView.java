package com.demo.ui.views;

import com.demo.auditing.model.CustomRevisionEntity;
import com.demo.auditing.service.AuditManager;
import com.demo.ui.MainLayout;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.HeaderRow;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;
import org.apache.commons.lang3.StringUtils;
import org.vaadin.gatanaso.MultiselectComboBox;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Route(value = "audits", layout = MainLayout.class)
public class AuditLogView  extends VerticalLayout {

    private final String SL_NO = getTranslation("customrevision.sl_no");
    private final String USER_NAME = getTranslation("customrevision.username");
    private final String KEY = getTranslation("customrevision.key");
    private final String REV_TYPE = getTranslation("customrevision.revtype");
    private final String MODIFIED_ENTITY = getTranslation("customrevision.modifiedentity");
    private AuditManager auditManager;

    public AuditLogView(AuditManager auditManager){
        this.auditManager = auditManager;
        PaginatedGrid<CustomRevisionEntity> grid = new PaginatedGrid<>();
        List<CustomRevisionEntity> customRevisionEntities = auditManager.findAll();
        grid.addColumn(item-> customRevisionEntities.indexOf(item) + 1).setHeader(SL_NO);
        grid.addColumn(item -> String.join(",", convertClassNamesToEntity(item.getModifiedEntityNames())))
                .setHeader(MODIFIED_ENTITY)
                .setWidth("15vw")
                .setSortable(true).setKey("entity").setResizable(true);
        grid.addColumn(CustomRevisionEntity::getKey).setKey("key")
                .setWidth("10vw").setHeader(KEY).setSortable(true).setResizable(true);
        grid.addColumn(item -> {
            String type = "MODIFIED";
            if(item.getRevtype().equalsIgnoreCase("add")){
                type = "ADDED";
            }else if(item.getRevtype().equalsIgnoreCase("del")){
                type = "DELETED";
            }
            return type;
        }).setHeader(REV_TYPE).setResizable(true)
                .setWidth("15vw").setSortable(true).setKey("type");
        grid.addColumn(CustomRevisionEntity::getUsername).setHeader(USER_NAME)
                .setWidth("10vw").setResizable(true)
                .setSortable(true).setKey("modifiedBy");
        grid.setHeight("80vh");
        Grid.Column historyColumn = grid.addColumn(new ComponentRenderer<>(item -> {
            Icon viewDetailsBtn = VaadinIcon.RECORDS.create();
            viewDetailsBtn.setColor("cornflowerblue");
            viewDetailsBtn.addClickListener(click -> {
                UI.getCurrent().navigate(AuditHistory.class, String.valueOf(item.getId()));
            });

            HorizontalLayout editLayout = new HorizontalLayout(viewDetailsBtn);
            editLayout.setJustifyContentMode(JustifyContentMode.CENTER);
            editLayout.setWidth("100%");
            return editLayout;
        })).setResizable(true);

        historyColumn.setKey("custom-revision-history")
                .setHeader("History");

        grid.setItems(customRevisionEntities);
        ListDataProvider<CustomRevisionEntity> dataProvider = new ListDataProvider<>(customRevisionEntities);
        grid.setDataProvider(dataProvider);
        // Sets the max number of items to be rendered on the grid for each page
        grid.setPageSize(12);

        // Sets how many pages should be visible on the pagination before and/or after the current selected page
        grid.setPaginatorSize(3);
        addFilters(grid, dataProvider);
        add(grid);
    }

    public void addFilters(Grid grid, ListDataProvider<CustomRevisionEntity> dataProvider){
        HeaderRow filterRow = grid.appendHeaderRow();

        Set<String> typeList = new HashSet<>();
        typeList.add("MODIFIED");
        typeList.add("ADDED");
        typeList.add("DELETED");

        Set<String> entityType = new HashSet<>();
        entityType.add("Party");
        entityType.add("User");
        entityType.add("Report");
        entityType.add("ReportAttribute");

        //Key filter
        TextField keyFilter = new TextField();
        keyFilter.addValueChangeListener(event -> {
            dataProvider.addFilter(
                    customRevision -> StringUtils.containsIgnoreCase(customRevision.getKey(),
                            keyFilter.getValue()));
        });
        keyFilter.setValueChangeMode(ValueChangeMode.EAGER);

        filterRow.getCell(grid.getColumnByKey("key")).setComponent(keyFilter);
        keyFilter.setSizeFull();
        keyFilter.setPlaceholder("Key Filter");

        //Modified By filter
        TextField modified = new TextField();
        modified.addValueChangeListener(event -> {
            dataProvider.addFilter(
                    customRevision -> StringUtils.containsIgnoreCase(customRevision.getUsername(),
                            modified.getValue()));
        });
        modified.setValueChangeMode(ValueChangeMode.EAGER);

        filterRow.getCell(grid.getColumnByKey("modifiedBy")).setComponent(modified);
        modified.setSizeFull();
        modified.setPlaceholder("Modified Filter");

        //Type
        ComboBox<String> typeField = new ComboBox<>();
        typeField.setItems(typeList);
        typeField.addValueChangeListener(event -> dataProvider.addFilter(
                customRevision ->
                    StringUtils
                        .containsIgnoreCase(typeField.getValue(), customRevision.getRevtype())
                        || typeField.getValue() == null ||typeField.getValue().isEmpty()
                ));


        MultiselectComboBox<String> multiselectComboBox = new MultiselectComboBox<>();
        multiselectComboBox.setItems(typeList);
        multiselectComboBox.setItemLabelGenerator(type -> type);
        multiselectComboBox.addSelectionListener(e -> {dataProvider.addFilter(
                customRevision -> {
                    Set<String> selectedTypes = multiselectComboBox.getValue();
                    if(selectedTypes.isEmpty()) return true;
                    String type = customRevision.getRevtype();
                    return selectedTypes.stream().anyMatch(selectedType -> StringUtils
                            .containsIgnoreCase(selectedType, type));
                });
        });
        multiselectComboBox.setSizeFull();
        multiselectComboBox.setPlaceholder("Type Filter");
        filterRow.getCell(grid.getColumnByKey("type")).setComponent(multiselectComboBox);


        //Entity
        MultiselectComboBox<String> entityMultiselectComboBox = new MultiselectComboBox<>();
        entityMultiselectComboBox.setItems(entityType);
        entityMultiselectComboBox.setItemLabelGenerator(entity -> entity);
        entityMultiselectComboBox.addSelectionListener(e -> {dataProvider.addFilter(
                customRevision -> {
                    Boolean result = false;
                    Set<String> selectedEntities = entityMultiselectComboBox.getValue();
                    if(selectedEntities.isEmpty()) return true;
                    Set<String> revisionEntities = convertClassNamesToEntity(customRevision.getModifiedEntityNames());
                    // Displaying the values after iterating through the iterator
                    for (String entity : selectedEntities) {
                        result = revisionEntities.stream().anyMatch(name -> name.equals(entity));
                        if(result) break;
                    }
                    return result;
                });
        });
        entityMultiselectComboBox.setSizeFull();
        entityMultiselectComboBox.setPlaceholder("Entity Field");
        filterRow.getCell(grid.getColumnByKey("entity")).setComponent(entityMultiselectComboBox);
    }



    private Set<String> convertClassNamesToEntity(Set<String> entities){
        Set<String> result = new HashSet<>();
        for(String entity: entities){
            if(entity == null)
                return result;

            String str[] = entity.trim().split("\\.");
            if(str.length > 0)
                result.add(str[str.length - 1]);
            else
                result.add(entity);

        }
        return result;
    }
}
