package com.demo.ui.views;

import com.amazonaws.services.s3.model.ObjectMetadata;
import com.demo.common.model.Report;
import com.demo.common.model.ReportAttribute;
import com.demo.common.model.Role;
import com.demo.common.service.AWSService;
import com.demo.common.service.ReportManager;
import com.demo.common.service.RoleManager;
import com.demo.ui.MainLayout;
import com.demo.dataprovider.ReportDataProvider;
import com.vaadin.flow.component.crud.*;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.upload.SucceededEvent;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.MemoryBuffer;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.Route;
import org.apache.commons.io.FileUtils;
import org.vaadin.gatanaso.MultiselectComboBox;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

@Route(value = "managereports", layout = MainLayout.class)
public class ManageReportView extends VerticalLayout {

    private AWSService awsService;

    private RoleManager roleManager;
    private final String REPORT_NAME = getTranslation("report.name");
    private final String ATTRIBUTE = getTranslation("report.attributes");
    private final String PERMISSION = getTranslation("report.permission");
    private final String FILE_NAME = getTranslation("report.fileName");
    private final String FILE_PATH = getTranslation("report.filePath");
    private final String NEW_REPORT = getTranslation("report.btn.newReport");
    
    public ManageReportView(AWSService awsService, ReportManager reportManager, RoleManager roleManager) {
        this.awsService = awsService;
        this.roleManager = roleManager;
        CrudGrid<Report> crudGrid = new CrudGrid<>(Report.class, false);

        List<Role> roles = roleManager.findAll();

        Crud<Report> crud = new Crud<>(Report.class, crudGrid, createEditor(roles));
        ReportDataProvider dataProvider = new ReportDataProvider(reportManager);

        crud.setDataProvider(dataProvider);
        crud.addSaveListener(e -> dataProvider.persist(e.getItem()));
        crud.addDeleteListener(e -> dataProvider.delete(e.getItem()));
        CrudI18n i18n = CrudI18n.createDefault();
        i18n.setNewItem(NEW_REPORT);
        crud.setI18n(i18n);

        crud.getGrid().removeAllColumns();
        crud.getGrid().addColumn(Report::getName).setHeader(REPORT_NAME);
        crud.getGrid().addColumn(item -> parseAttributes(item.getReportAttributes())).setHeader(ATTRIBUTE);
        crud.getGrid().addColumn(Report::getPermission).setHeader(PERMISSION);
        crud.getGrid().addColumn(Report::getFileName).setHeader(FILE_NAME);

        crud.addThemeVariants(CrudVariant.NO_BORDER);

        Crud.addEditColumn(crud.getGrid());

        crud.getGrid().addItemDoubleClickListener(
                e -> crud.edit(e.getItem(), Crud.EditMode.EXISTING_ITEM));

        add(crud);
    }

    private CrudEditor<Report> createEditor(List<Role> roleList) {
        TextField fileName = new TextField(FILE_NAME);
        fileName.setEnabled(false);
        TextField filePath = new TextField(FILE_PATH);
        TextField name = new TextField();
        TextField attributes = new TextField();

        MemoryBuffer buffer = new MemoryBuffer();
        Upload upload = new Upload(buffer);

        upload.addSucceededListener(event -> {
            String path = uploadToLocalStorage(event, buffer);
            filePath.setValue(path);
            fileName.setValue(event.getFileName());
        });

        Set<Role> roleSet = roleList.stream().collect(Collectors.toSet());

        MultiselectComboBox<Role> multiselectComboBox = new MultiselectComboBox<>();
        multiselectComboBox.setItems(roleSet);
        multiselectComboBox.setItemLabelGenerator(role -> role.getName());

        FormLayout formLayout = new FormLayout();
        formLayout.setResponsiveSteps(new FormLayout.ResponsiveStep("0", 1, FormLayout.ResponsiveStep.LabelsPosition.TOP),
                new FormLayout.ResponsiveStep("600px", 1, FormLayout.ResponsiveStep.LabelsPosition.ASIDE));
        formLayout.addFormItem(name, FILE_NAME);
        formLayout.addFormItem(attributes, ATTRIBUTE);
        formLayout.addFormItem(multiselectComboBox, PERMISSION);
        formLayout.add(upload);

        Binder<Report> binder = new Binder<>(Report.class);
        binder.forField(multiselectComboBox).bind(report -> rolesOf(report.getPermission(),roleSet),((report, roles) -> {report.setPermission(getRoleName(roles));}));
        binder.bind(fileName, Report::getFileName, Report::setFileName);
        binder.bind(name, Report::getName, Report::setName);
        binder.bind(filePath, Report::getFilePath, Report::setFilePath);
        binder.bind(attributes,report -> parseAttributes(report.getReportAttributes()),((report, s) -> report.setReportAttributes(createAttributes(s,report))));

        return new BinderCrudEditor<>(binder, formLayout);
    }

    private String getRoleName(Set<Role> rolesSet){

        StringBuilder roles = new StringBuilder("");
        rolesSet.forEach(role -> {
            if (roles.length()>0) roles.append(",");
            roles.append(role.getName());
        });
        return roles.toString();
    }

    private Role roleOf(String permission){
        Role role = roleManager.findByName(permission);
        System.out.println("Role " + role.getName());
        return role;
    }

    private Set<Role> rolesOf(String permission, Set<Role> roleList){

        Set<Role> collect = new HashSet<>();
        if(permission != null && !permission.isEmpty()) {

            collect = roleList.stream().filter(role -> {
                return Arrays.asList(permission.split(",")).contains(role.getName()) ? true : false;
            }).collect(Collectors.toSet());
        }
        return collect;
    }

    private String parseAttributes(List<ReportAttribute> attributes){
        StringBuilder repoAttrs = new StringBuilder("");
        for (ReportAttribute attribute : attributes) {
            if (repoAttrs.length()>0) repoAttrs.append(",");
            repoAttrs.append(attribute.getName());
        }
        return repoAttrs.toString();
    }

    private List<ReportAttribute> createAttributes(String attributes, Report report){
        List<ReportAttribute> reportAttributes = new ArrayList<>();
        for (String s : attributes.split(",")) {
            ReportAttribute reportAttribute = new ReportAttribute();
            reportAttribute.setName(s);
            reportAttribute.setReport(report);
            reportAttributes.add(reportAttribute);
        }
        return reportAttributes;
    }

    private String uploadToAws(SucceededEvent event, MemoryBuffer buffer){
        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.setContentType(event.getMIMEType());
        String name = "vaadin_reports/"+event.getFileName();
        return awsService.putObject(name, buffer.getInputStream(), objectMetadata);
    }

    private String uploadToLocalStorage(SucceededEvent event, MemoryBuffer buffer){
        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.setContentType(event.getMIMEType());
        String uuidToken = UUID.randomUUID().toString();
        String path = "../resources/JasperReportGenerator/Documents/"+ uuidToken+ "_" +event.getFileName();
        try {
            File targetFile = new File(path);
            FileUtils.copyInputStreamToFile(buffer.getInputStream(), targetFile);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return path;
    }
}
