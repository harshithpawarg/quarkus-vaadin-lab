package com.demo.ui.views;


import com.demo.common.model.Party;
import com.demo.common.notifications.Broadcaster;
import com.demo.common.service.PartyManager;
import com.demo.ui.MainLayout;
import com.demo.ui.util.DataTypeConverter;
import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.DetachEvent;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.crud.BinderCrudEditor;
import com.vaadin.flow.component.crud.Crud;
import com.vaadin.flow.component.crud.CrudEditor;
import com.vaadin.flow.component.crud.CrudI18n;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridSortOrder;
import com.vaadin.flow.component.grid.HeaderRow;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.data.provider.SortDirection;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.shared.Registration;
//import com.vaadin.flow.spring.annotation.UIScope;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Route(value = "party", layout = MainLayout.class)
//@UIScope
@CssImport(value = "./styles/styles.css", themeFor = "vaadin-grid")
public class PartyView extends VerticalLayout {

    PartyManager partyManager;
    Registration broadcasterRegistration;

    private final String PARTY_NAME = getTranslation("party.name");
    private final String BUSINESS_LEGAL_NAME = getTranslation("party.businessLegalName");
    private final String EMAIL = getTranslation("party.email");
    private final String IS_FUNDER = getTranslation("party.isFunder");
    private final String ENABLE = getTranslation("party.enable");
    private final String NEW_PARTY = getTranslation("party.label.newParty");
    private final String ADD_PARTY_BTN = getTranslation("party.btn.newParty");
    private final String FILTER_PLACEHOLDER = getTranslation("party.filter");

    public PartyView(PartyManager partyManager){
        this.partyManager = partyManager;
        addComponents();
    }

    private void addComponents() {
        ListDataProvider<Party> partyListDataProvider = createDataProvider();
        Grid<Party> partyGrid = createPartyGrid(partyListDataProvider);
        CrudEditor<Party> partyCrudEditor = createPartyEditor();
        Crud<Party> partyCrud = createPartyCrud(partyGrid,partyCrudEditor);
        add(partyCrud);
    }

    private ListDataProvider<Party> createDataProvider() {
        List<Party> partyList = getPartyList();
        return new ListDataProvider<>(partyList);
    }

    private Grid<Party> createPartyGrid(ListDataProvider<Party> dataProvider){

        Grid<Party> partyGrid = new Grid<>();

        //Assign data provider to grid
        partyGrid.setDataProvider(dataProvider);

        //Specify Columns
        Grid.Column<Party> nameColumn = partyGrid
                .addColumn(Party::getName).setHeader(PARTY_NAME).setSortable(true);
        Grid.Column<Party> businessNameColumn = partyGrid.addColumn(Party::getBusinessLegalName)
                .setHeader(BUSINESS_LEGAL_NAME).setSortable(true);
        Grid.Column<Party> emailColumn = partyGrid.addColumn(Party::getEmail)
                .setHeader(EMAIL).setSortable(true);
        Grid.Column<Party> isFunderColumn = partyGrid.addColumn(item -> DataTypeConverter.ConvertBoolToString(item.getIsFunder()))
                .setHeader(IS_FUNDER).setSortable(true);
        Grid.Column<Party> enableColumn = partyGrid.addColumn(item -> DataTypeConverter.ConvertBoolToString(item.getEnable()))
                .setHeader(ENABLE).setSortable(true);


        //Default Sort order
        List<GridSortOrder<Party>> sort = new ArrayList<>();
        sort.add(new GridSortOrder<>(nameColumn, SortDirection.ASCENDING));
        partyGrid.sort(sort);

        //Highlight rows with inactive users
        partyGrid.setClassNameGenerator(party -> {
            if (!party.getEnable()) {
                return "in-active-party";
            }
            return "";
        });

        //Add filters for columns
        HeaderRow filterRow = partyGrid.appendHeaderRow();

        // Name filter
        TextField nameField = new TextField();
        nameField.addValueChangeListener(event -> dataProvider.addFilter(
                party -> StringUtils.containsIgnoreCase(party.getName(),
                        nameField.getValue())));

        nameField.setValueChangeMode(ValueChangeMode.EAGER);

        filterRow.getCell(nameColumn).setComponent(nameField);
        nameField.setSizeFull();
        nameField.setPlaceholder(FILTER_PLACEHOLDER);

        // Business Nmae filter
        TextField businessNameField = new TextField();
        businessNameField.addValueChangeListener(event -> dataProvider.addFilter(
                party -> StringUtils.containsIgnoreCase(party.getBusinessLegalName(),
                        businessNameField.getValue())));

        businessNameField.setValueChangeMode(ValueChangeMode.EAGER);

        filterRow.getCell(businessNameColumn).setComponent(businessNameField);
        businessNameField.setSizeFull();
        businessNameField.setPlaceholder(FILTER_PLACEHOLDER);

        // Email filter
        TextField emailField = new TextField();
        emailField.addValueChangeListener(event -> dataProvider.addFilter(
                party -> StringUtils.containsIgnoreCase(party.getEmail(),
                        emailField.getValue())));

        emailField.setValueChangeMode(ValueChangeMode.EAGER);

        filterRow.getCell(emailColumn).setComponent(emailField);
        emailField.setSizeFull();
        emailField.setPlaceholder(FILTER_PLACEHOLDER);

        // Is Funder filter
        ComboBox<String> isFunderField = new ComboBox<>();
        isFunderField.setItems("Yes","No");
        isFunderField.addValueChangeListener(event -> dataProvider.addFilter(
                party -> ( party.getIsFunder() == DataTypeConverter.ConvertStringToBool(isFunderField.getValue()))));

        filterRow.getCell(isFunderColumn).setComponent(isFunderField);
        isFunderField.setSizeFull();
        isFunderField.setPlaceholder(FILTER_PLACEHOLDER);

        //Is Active filter
        ComboBox<String> isActiveField = new ComboBox<>();
        isActiveField.setItems("Yes","No");
        isActiveField.addValueChangeListener(event -> dataProvider.addFilter(
                party -> ( party.getEnable() == DataTypeConverter.ConvertStringToBool(isActiveField.getValue()))));

        filterRow.getCell(enableColumn).setComponent(isActiveField);
        isActiveField.setSizeFull();
        isActiveField.setPlaceholder(FILTER_PLACEHOLDER);

        return  partyGrid;
    }

    private CrudEditor<Party> createPartyEditor(){

        TextField name = new TextField(PARTY_NAME);
        TextField businessLegalName = new TextField(BUSINESS_LEGAL_NAME);
        EmailField email = new EmailField(EMAIL);
        Checkbox isFunder = new Checkbox(IS_FUNDER);
        Checkbox isActive = new Checkbox(ENABLE);

        FormLayout formLayout = new FormLayout();
        formLayout.setResponsiveSteps(
                new FormLayout.ResponsiveStep("50em", 1),
                new FormLayout.ResponsiveStep("25em", 2),
                new FormLayout.ResponsiveStep("25em", 3));
        formLayout.add(name,businessLegalName,email, isFunder,isActive);
        formLayout.setColspan(businessLegalName,2);

        Binder<Party> binder = new Binder<>(Party.class);

        binder.bind(name, Party::getName, Party::setName);
        binder.bind(businessLegalName, Party::getBusinessLegalName, Party::setBusinessLegalName);
        binder.bind(email, Party::getEmail, Party::setEmail);
        binder.bind(isFunder, Party::getIsFunder, Party::setIsFunder);
        binder.bind(isActive, Party::getEnable, Party::setEnable);

        return new BinderCrudEditor<>(binder,formLayout);
    }

    private Crud<Party> createPartyCrud(Grid<Party> partyGrid , CrudEditor<Party> partyCrudEditor) {
        Crud<Party> partyCrud = new Crud<>(Party.class,partyGrid,partyCrudEditor);

        partyCrud.addSaveListener(e -> saveParty(e.getItem()));
        partyCrud.addDeleteListener(e -> deleteParty(e.getItem()));

        Crud.addEditColumn(partyGrid);
        partyCrud.getGrid().addItemDoubleClickListener(
                e -> partyCrud.edit(e.getItem(), Crud.EditMode.EXISTING_ITEM));
        partyCrud.getGrid().addComponentColumn(item -> new Button(VaadinIcon.MINUS_CIRCLE.create(), click -> {
            deleteParty(item);
        })).setFlexGrow(0);

        //Custom Footer
        Span footer = new Span();
        footer.getElement().getStyle().set("flex", "1");

        Button newItemButton = new Button(ADD_PARTY_BTN);
        newItemButton.addClickListener(e -> partyCrud.edit(new Party(), Crud.EditMode.NEW_ITEM));
        partyCrud.setToolbar(footer, newItemButton);

        //Customize the CRUD Component labels
        CrudI18n crudI18n = new CrudI18n().createDefault();
        crudI18n.setNewItem(NEW_PARTY);
        partyCrud.setI18n(crudI18n);

        return partyCrud;
    }

    private List<Party> getPartyList(){
        return this.partyManager.findAll();
    }

    private void saveParty(Party party){
        partyManager.savePartyDetails(party);
    }

    private void deleteParty(Party party){
        this.partyManager.deleteParty(party);
    }

    @Override
    protected void onAttach(AttachEvent attachEvent) {
        UI ui = attachEvent.getUI();
        broadcasterRegistration = Broadcaster.register(partyList -> {
            ui.access(() -> {
                        Crud<Party> partyCrud1 = (Crud<Party>) this.getComponentAt(0);
                        this.remove(partyCrud1);
                        Crud<Party> newCrud = createPartyCrud(createPartyGrid(new ListDataProvider<>(partyList)),createPartyEditor());
                         this.add(newCrud);
            }
            );
        });
    }

    @Override
    protected void onDetach(DetachEvent detachEvent) {
        broadcasterRegistration.remove();
        broadcasterRegistration = null;
    }
}
