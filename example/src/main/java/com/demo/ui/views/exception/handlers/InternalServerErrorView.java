package com.demo.ui.views.exception.handlers;

import com.demo.ui.MainLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

@Route(value = "error", layout = MainLayout.class)
public class InternalServerErrorView extends VerticalLayout {
    public InternalServerErrorView(){
        add(new CustomDivComponent("500 - Internal Server Error"));
    }
}
