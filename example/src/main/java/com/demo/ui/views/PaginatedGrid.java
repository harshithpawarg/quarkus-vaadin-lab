package com.demo.ui.views;


import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.data.provider.Query;
import com.vaadin.flow.data.provider.QuerySortOrder;
import com.vaadin.flow.dom.Element;
import com.vaadin.flow.function.SerializableComparator;
import com.vaadin.flow.shared.Registration;

import java.util.List;
import java.util.Objects;


public class PaginatedGrid<T> extends Grid<T> {
    private LitCrudPagination paginaton = new LitCrudPagination();
    private DataProvider<T, ?> dataProvider = super.getDataProvider();

    public PaginatedGrid() {
        this.setHeightByRows(true);
        this.paginaton.addPageChangeListener((e) -> {
            this.doCalcs(e.getNewPage());
            this.paginaton.setPage(e.getNewPage());
        });
    }

    protected void onAttach(AttachEvent attachEvent) {
        super.onAttach(attachEvent);
        this.addSortListener(sort ->{
            doCalcs(this.paginaton.getPage());
        });
        this.getParent().ifPresent((p) -> {
            int indexOfChild = p.getElement().indexOfChild(this.getElement());
            Span wrapper = new Span(new Component[]{this.paginaton});
            wrapper.getElement().getStyle().set("width", "100%");
            wrapper.getElement().getStyle().set("display", "flex");
            wrapper.getElement().getStyle().set("justify-content", "center");
            p.getElement().insertChild(indexOfChild + 1, new Element[]{wrapper.getElement()});
        });
        this.doCalcs(0);
    }

    private void doCalcs(int newPage) {
        int offset = newPage > 0 ? (newPage - 1) * this.getPageSize() : 0;
        PaginatedGrid.InnerQuery query = new PaginatedGrid.InnerQuery(offset, this.getSortOrder());
        this.paginaton.setTotal(this.dataProvider.size(query));
        super.setDataProvider(DataProvider.fromStream(this.dataProvider.fetch(query)));
    }

    public void refreshPaginator() {
        if (this.paginaton != null) {
            this.paginaton.setPageSize(this.getPageSize());
            this.paginaton.setPage(1);
            if (this.dataProvider != null) {
                this.doCalcs(this.paginaton.getPage());
            }

            this.paginaton.refresh();
        }

    }

    public void setPageSize(int pageSize) {
        super.setPageSize(pageSize);
        this.refreshPaginator();
    }

    public void setPage(int page) {
        this.paginaton.setPage(page);
    }

    public int getPage() {
        return this.paginaton.getPage();
    }

    public void setHeightByRows(boolean heightByRows) {
        super.setHeightByRows(true);
    }

    public void setPaginatorSize(int size) {
        this.paginaton.setPage(1);
        this.paginaton.setPaginatorSize(size);
        this.paginaton.refresh();
    }

    public void setPaginatorTexts(String pageText, String ofText) {
        this.paginaton.setPageText(pageText);
        this.paginaton.setOfText(ofText);
    }

    public void setDataProvider(DataProvider<T, ?> dataProvider) {
        Objects.requireNonNull(dataProvider, "DataProvider shoul not be null!");
        if (!Objects.equals(this.dataProvider, dataProvider)) {
            this.dataProvider = dataProvider;
            this.dataProvider.addDataProviderListener((event) -> {
                this.refreshPaginator();
            });
            this.refreshPaginator();
        }

    }

    public Registration addPageChangeListener(ComponentEventListener<LitCrudPagination.PageChangeEvent> listener) {
        return this.paginaton.addPageChangeListener(listener);
    }

    private class InnerQuery<F> extends Query<T, F> {
        InnerQuery() {
            this(0);
        }

        InnerQuery(int offset) {
            super(offset, PaginatedGrid.this.getPageSize(), PaginatedGrid.this.getDataCommunicator().getBackEndSorting(), PaginatedGrid.this.getDataCommunicator().getInMemorySorting(), null);
        }

        InnerQuery(int offset, List<QuerySortOrder> sortOrders, SerializableComparator<T> serializableComparator) {
            super(offset, PaginatedGrid.this.getPageSize(), sortOrders, serializableComparator, null);
        }

        InnerQuery(int offset, List<QuerySortOrder> sortOrders) {
            super(offset, PaginatedGrid.this.getPageSize(), sortOrders,  PaginatedGrid.this.getDataCommunicator().getInMemorySorting(), null);
        }
    }
}

