package com.demo.ui.views;


import com.demo.common.model.Party;
import com.demo.common.service.PartyManager;
import com.demo.ui.MainLayout;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.gridpro.GridPro;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.shared.Registration;
//import com.vaadin.flow.spring.annotation.UIScope;

import java.util.ArrayList;
import java.util.List;

@Route(value = "party_pro", layout = MainLayout.class)
//@UIScope
@CssImport(value = "./styles/styles.css", themeFor = "vaadin-grid")
public class PartyWithGridPro extends VerticalLayout {

    PartyManager partyManager;
    Registration broadcasterRegistration;

    private final String PARTY_NAME = getTranslation("party.name");
    private final String BUSINESS_LEGAL_NAME = getTranslation("party.businessLegalName");
    private final String EMAIL = getTranslation("party.email");
    private final String IS_FUNDER = getTranslation("party.isFunder");
    private final String ENABLE = getTranslation("party.enable");
    private final String NEW_PARTY = getTranslation("party.label.newParty");
    private final String ADD_PARTY_BTN = getTranslation("party.btn.newParty");
    private final String FILTER_PLACEHOLDER = getTranslation("party.filter");

    public PartyWithGridPro(PartyManager partyManager){
        this.partyManager = partyManager;
        addComponents();
    }

    private void addComponents() {
        GridPro<Party> partyGrid = createPartyGrid();
        add(partyGrid);
    }
    
    private GridPro<Party> createPartyGrid(){

        GridPro<Party> grid = new GridPro<>();
        grid.setItems(getPartyList());

        /*
         * Using EditColumnConfigurator it is possible to define the type of the editor:
         * "text", "checkbox" or "select" and provide needed parameters.
         */
        grid.addEditColumn(Party::getName)
                .text((item, newValue) ->
                        item.setName(newValue))
                .setHeader(PARTY_NAME);

        grid.addEditColumn(Party::getBusinessLegalName)
                .text((item, newValue) ->
                        item.setBusinessLegalName(newValue))
                .setHeader(BUSINESS_LEGAL_NAME);

        grid.addEditColumn(Party::getEmail)
                .text((item, newValue) ->
                        item.setEmail(newValue))
                .setHeader(EMAIL);

        grid.addEditColumn(Party::getIsFunder)
                .checkbox((item, newValue) ->
                        item.setIsFunder(newValue))
                .setHeader(IS_FUNDER);


        List<String> optionsList = new ArrayList<>();
        optionsList.add("true");
        optionsList.add("false");
        grid.addEditColumn(Party::getEnable)
                .select((item, newValue) ->
                        item.setEnable(stringBooleanToString(newValue)), optionsList)
                .setHeader(ENABLE);

        grid.addColumn(
                new ComponentRenderer<>(item -> {
                    Button saveBtn = new Button("Save");
                    saveBtn.addClickListener(click -> {
                        saveParty(item);
                    });
                    return saveBtn;
                })
        ).setHeader("Action");


        /*
         * It is possible to allow enter pressing change the row by using grid pro method setEnterNextRow.
         */
        grid.setEnterNextRow(true);
        return  grid;
    }

    public Boolean stringBooleanToString(String newValue){
        return newValue.equalsIgnoreCase("true");
    }

    private void saveParty(Party party){
        partyManager.savePartyDetails(party);
    }

    private List<Party> getPartyList(){
        return this.partyManager.findAll();
    }

}
