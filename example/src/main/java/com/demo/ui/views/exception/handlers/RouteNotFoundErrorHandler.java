package com.demo.ui.views.exception.handlers;

import com.demo.ui.MainLayout;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.router.*;

import javax.servlet.http.HttpServletResponse;

@Tag(Tag.DIV)
@ParentLayout(MainLayout.class)
public class RouteNotFoundErrorHandler extends Component
        implements HasErrorParameter<NotFoundException> {

    @Override
    public int setErrorParameter(BeforeEnterEvent event,
                                 ErrorParameter<NotFoundException> parameter) {
        getElement().setText("404 - page not found")
                .getStyle()
                .set("width","100%")
                .set("text-align","center")
                .set("font-weight","bold")
                .set("font-size","40px");;

        return HttpServletResponse.SC_NOT_FOUND;
    }
}