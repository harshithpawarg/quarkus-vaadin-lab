package com.demo.ui.views.exception.handlers;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Tag;

@Tag(Tag.DIV)
public class CustomDivComponent extends Component {

    public CustomDivComponent(String error){
        getElement().setText(error)
                .getStyle()
                .set("width","100%")
                .set("text-align","center")
                .set("font-weight","bold")
                .set("font-size","40px");
    }
}
