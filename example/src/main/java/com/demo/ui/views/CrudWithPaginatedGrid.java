package com.demo.ui.views;

import com.demo.ui.views.LitCrudPagination.PageChangeEvent;
import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.crud.Crud;
import com.vaadin.flow.component.crud.CrudEditor;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.data.provider.Query;
import com.vaadin.flow.data.provider.QuerySortOrder;
import com.vaadin.flow.function.SerializableComparator;
import com.vaadin.flow.shared.Registration;

import java.util.List;
import java.util.Objects;

public class CrudWithPaginatedGrid<T> extends Crud<T> {
    private LitCrudPagination paginaton = new LitCrudPagination();
    private DataProvider<T, ?> dataProvider = super.getDataProvider();
    private Button addButton;

    public CrudWithPaginatedGrid(Class<T> beanType, Grid<T> grid, CrudEditor<T> editor, Button addButton) {
        super(beanType, grid, editor);
        this.setHeightByRows(true);
        this.addButton = addButton;
        this.paginaton.addPageChangeListener((e) -> {
            this.doCalcs(e.getNewPage());
            this.paginaton.setPage(e.getNewPage());
        });
    }

    public CrudWithPaginatedGrid(Class<T> beanType, CrudEditor<T> editor) {
        super(beanType, editor);
        this.setHeightByRows(true);

        this.paginaton.addPageChangeListener((e) -> {
            this.doCalcs(e.getNewPage());
            this.paginaton.setPage(e.getNewPage());
        });
    }

    protected void onAttach(AttachEvent attachEvent) {
        super.onAttach(attachEvent);
        this.getParent().ifPresent((p) -> {
//            int indexOfChild = p.getElement().indexOfChild(this.getElement());
//            Span wrapper = new Span(new Component[]{this.paginaton});
//            wrapper.getElement().getStyle().set("width", "100%");
//            wrapper.getElement().getStyle().set("display", "flex");
//            wrapper.getElement().getStyle().set("justify-content", "center");
//            p.getElement().insertChild(indexOfChild + 1, new Element[]{wrapper.getElement()});
            this.getGrid().addSortListener(sort ->{
                 doCalcs(this.paginaton.getPage());
            });
            HorizontalLayout layout = new HorizontalLayout();
            layout.setWidthFull();
            layout.setJustifyContentMode(FlexComponent.JustifyContentMode.BETWEEN);
            layout.add(this.paginaton, addButton);
            setToolbar(layout);
        });
        this.doCalcs(0);
    }

    private void doCalcs(int newPage) {
        int offset = newPage > 0 ? (newPage - 1) * this.getGrid().getPageSize() : 0;
        CrudWithPaginatedGrid.InnerQuery query = new CrudWithPaginatedGrid.InnerQuery(offset, this.getGrid().getSortOrder());
        this.paginaton.setTotal(this.dataProvider.size(query));
        super.setDataProvider(DataProvider.fromStream(this.dataProvider.fetch(query)));
    }

    public void refreshPaginator() {
        if (this.paginaton != null) {
            this.paginaton.setPageSize(this.getGrid().getPageSize());
            this.paginaton.setPage(1);
            if (this.dataProvider != null) {
                this.doCalcs(this.paginaton.getPage());
            }

            this.paginaton.refresh();
        }

    }

    public void setPageSize(int pageSize) {
        super.getGrid().setPageSize(pageSize);
        this.refreshPaginator();
    }

    public void setPage(int page) {
        this.paginaton.setPage(page);
    }

    public int getPage() {
        return this.paginaton.getPage();
    }

    public void setHeightByRows(boolean heightByRows) {
        super.getGrid().setHeightByRows(true);
    }

    public void setPaginatorSize(int size) {
        this.paginaton.setPage(1);
        this.paginaton.setPaginatorSize(size);
        this.paginaton.refresh();
    }

    public void setPaginatorTexts(String pageText, String ofText) {
        this.paginaton.setPageText(pageText);
        this.paginaton.setOfText(ofText);
    }

    public void setDataProvider(DataProvider<T, ?> dataProvider) {
        Objects.requireNonNull(dataProvider, "DataProvider shoul not be null!");
        if (!Objects.equals(this.dataProvider, dataProvider)) {
            this.dataProvider = dataProvider;
            this.getGrid().getDataProvider().addDataProviderListener((event) -> {
                this.refreshPaginator();
            });
            this.refreshPaginator();
        }

    }

    public Registration addPageChangeListener(ComponentEventListener<PageChangeEvent> listener) {
        return this.paginaton.addPageChangeListener(listener);
    }

    private class InnerQuery<F> extends Query<T, F> {
        InnerQuery() {
            this(0);
        }

        InnerQuery(int offset) {
            super(offset, CrudWithPaginatedGrid.this.getGrid().getPageSize(), CrudWithPaginatedGrid.this.getGrid().getDataCommunicator().getBackEndSorting(), CrudWithPaginatedGrid.this.getGrid().getDataCommunicator().getInMemorySorting(), null);
        }

        InnerQuery(int offset, List<QuerySortOrder> sortOrders, SerializableComparator<T> serializableComparator) {
            super(offset, CrudWithPaginatedGrid.this.getGrid().getPageSize(), sortOrders, serializableComparator, null);
        }

        InnerQuery(int offset, List<QuerySortOrder> sortOrders) {
            super(offset, CrudWithPaginatedGrid.this.getGrid().getPageSize(), sortOrders,  CrudWithPaginatedGrid.this.getGrid().getDataCommunicator().getInMemorySorting(), null);
        }
    }
}
