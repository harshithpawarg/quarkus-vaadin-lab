package com.demo.ui.views;

import com.demo.common.model.Report;
import com.demo.common.model.ReportAttribute;
import com.demo.common.service.KeycloakService;
import com.demo.common.service.ReportManager;
import com.demo.common.service.UserManager;
import com.demo.ui.MainLayout;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.ColumnTextAlign;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.Page;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.StreamResource;

import java.io.ByteArrayInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

@Route(value = "reports", layout = MainLayout.class)
public class ReportView extends VerticalLayout {

    private final String DOWNLOAD_TEXT = getTranslation("btn.download");
    private final String CANCEL_BTN = getTranslation("btn.cancel");
    private final String REPORT_NAME = getTranslation("report.name");
    private final String GENERATE_REPORT_LABEL = getTranslation("report.generate.label");
    private final String SL_NO = getTranslation("report.sl_no");
    private final String GENERATE_BTN_HEADER = getTranslation("report.generatebtn.columnheader");

    private ReportManager reportManager;

    ReportView(ReportManager reportManager, UserManager userManager, KeycloakService keycloakService) {
        this.reportManager = reportManager;
        System.out.println("Gggggggggggggggggggrrrr");
        Grid<Report> grid = new Grid<>(Report.class);
        String email = keycloakService.getCurrentUserEmail();
        List<String> currentUserRoles = userManager.getRolesForEmail(email);
        System.out.println("currentUserRoles: " +currentUserRoles);
        Set<Report> reportSet = new HashSet<>();
        for(String role: currentUserRoles) {
            reportSet.addAll(reportManager.findByPermissionContainingIgnoreCase(role));
        }
        List<Report> reports = new ArrayList<>();
        reports.addAll(reportSet);
        grid.setItems(reports);
        Grid.Column id = grid.addColumn(item -> reports.indexOf(item) + 1).setHeader(SL_NO).setWidth("50px").setFlexGrow(0);
        Grid.Column reportNameColumn = grid.getColumnByKey("name").setHeader(REPORT_NAME);
        reportNameColumn.setTextAlign(ColumnTextAlign.CENTER);
        grid.removeColumnByKey("id");
        grid.removeColumnByKey("createdAt");
        grid.removeColumnByKey("updatedAt");
        grid.removeColumnByKey("filePath");
        grid.removeColumnByKey("fileName");
        grid.removeColumnByKey("permission");
        grid.removeColumnByKey("reportAttributes");

// edit row btns
        Grid.Column buttonColumn = grid.addColumn(new ComponentRenderer<>(item -> {
            Button generateReportBtn = new Button(GENERATE_REPORT_LABEL, VaadinIcon.RECORDS.create());

            generateReportBtn.addClickListener(click -> {
                System.out.println("Generate Report" + item.getId());
                Dialog dialog = new Dialog();
                generateReportView(dialog, item, userManager);

            });

            generateReportBtn.setWidth("100%");

            HorizontalLayout editLayout = new HorizontalLayout(generateReportBtn);
            editLayout.setWidth("100%");
            return editLayout;
        }));
        buttonColumn.setKey("generatereport")
                .setHeader(GENERATE_BTN_HEADER)
                .setFlexGrow(0)
                .setWidth("300px")
                .setId("reportaction");

        grid.setColumnOrder(id, reportNameColumn, buttonColumn);
        add(grid);
    }

    private void generateReportView(Dialog dialog, Report selectedReport, UserManager userManager) {
        Report report = reportManager.getReport(selectedReport.getId());
        VerticalLayout verticalLayout = new VerticalLayout();

        List<ReportAttribute> attributes = report.getReportAttributes();

        Label label = new Label(GENERATE_REPORT_LABEL);
        label.getStyle().set("font-weight", "bold");
        HorizontalLayout actions = new HorizontalLayout();
        Button cancelButton = new Button(CANCEL_BTN);
        Button generateButton = new Button(GENERATE_REPORT_LABEL);
        actions.add(cancelButton, generateButton);
        verticalLayout.add(label);
        List<TextField> fields = new ArrayList<>();
        Map<String, TextField> attributesValues = new HashMap<>();
        for (ReportAttribute attribute : attributes){
            String []attr = attribute.getName().split(",");
            for(String name : attr) {
                TextField filed = new TextField();

                filed.setLabel(name);
                filed.setPlaceholder(name);
                verticalLayout.add(filed);
                fields.add(filed);
                attributesValues.put(name, filed);
            }
        }
        verticalLayout.add(actions);
        dialog.setCloseOnOutsideClick(true);
        dialog.add(verticalLayout);
        dialog.open();
        cancelButton.addClickListener(e->{
            dialog.close();
        });
        Map<String, Object> generatedAttributes = new HashMap<>();
        generateButton.addClickListener(e->{
            dialog.close();
            for (ReportAttribute attribute : attributes){
                String []attr = attribute.getName().split(",");
                for(String key : attr) {
                    String value = attributesValues.get(key).getValue();
                    generatedAttributes.put(key, value);
                }
            }

            try {
                String path = userManager.generateLetter(report, generatedAttributes);
                byte[] arr = Files.readAllBytes(Paths.get(path));
                String date = new Date().toString();
                String fileName = path.replace("../resources/jasperReport/", "");
                StreamResource resource = new StreamResource(fileName, () -> new ByteArrayInputStream(arr));
                Anchor downloadLink = new Anchor(resource, DOWNLOAD_TEXT);
                downloadLink.setId(date);
                downloadLink.getElement().getStyle().set("display", "none");
                downloadLink.getElement().setAttribute("download", true);
                add(downloadLink);
                Page page = UI.getCurrent().getPage();
                page.executeJavaScript("document.getElementById('"+date+"').click();");

            }catch (Exception ex){
                ex.printStackTrace();
            }
        });
    }
}
