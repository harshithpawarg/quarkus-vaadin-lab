package com.demo.ui.views.exception.handlers;

//import com.bugsnag.Bugsnag;
import com.demo.ui.MainLayout;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.ErrorParameter;
import com.vaadin.flow.router.HasErrorParameter;
import com.vaadin.flow.router.ParentLayout;
import org.springframework.beans.factory.annotation.Value;

import javax.servlet.http.HttpServletResponse;

@Tag(Tag.DIV)
@ParentLayout(MainLayout.class)
public class InternalServerErrorHandler extends Component
        implements HasErrorParameter<Exception> {

    @Value("${spring.bugsnag.apiKey}")
    private String apiKey;

    //Bugsnag bugsnag = new Bugsnag(apiKey);

    @Override
    public int setErrorParameter(BeforeEnterEvent event,
                                 ErrorParameter<Exception> parameter) {


        //bugsnag.notify(parameter.getException());
        parameter.getException().printStackTrace();
        event.rerouteTo(InternalServerErrorView.class);
        return HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
    }


}