package com.demo.ui.views;

import com.vaadin.flow.component.*;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.dependency.NpmPackage;
import com.vaadin.flow.component.dependency.NpmPackage.Container;
import com.vaadin.flow.shared.Registration;
import org.vaadin.klaudeta.LitPaginationModel;

import java.io.Serializable;

@Tag("lit-pagination")
@Container({@NpmPackage(
        value = "@polymer/paper-button",
        version = "^3.0.1"
), @NpmPackage(
        value = "@polymer/iron-iconset-svg",
        version = "^3.0.1"
), @NpmPackage(
        value = "@polymer/paper-icon-button",
        version = "^3.0.2"
), @NpmPackage(
        value = "lit-element",
        version = "^2.2.1"
), @NpmPackage(
        value = "lit-html",
        version = "^1.1.2"
)})
@JsModule("./lit-pagination.js")
public class LitCrudPagination extends Component implements LitPaginationModel {
    public LitCrudPagination() {
        this.setTotal(2);
        this.setPageSize(1);
        this.setSize(1);
    }

    public int getPageSize() {
        return this.getLimit();
    }

    public void setPageSize(int pageSize) {
        this.setLimit(pageSize);
    }

    public void setPaginatorSize(int size) {
        this.setSize(size);
    }

    public void refresh() {
        this.getElement().executeJs("$0.observePageCount($1,$2,$3)", new Serializable[]{this, this.getPage(), this.getPageSize(), this.getTotal()});
    }

    protected Registration addPageChangeListener(ComponentEventListener<PageChangeEvent> listener) {
        return super.addListener(LitCrudPagination.PageChangeEvent.class, listener);
    }

    @DomEvent("page-change")
    public static class PageChangeEvent extends ComponentEvent<LitCrudPagination> {
        private final int newPage;
        private final int oldPage;

        public PageChangeEvent(LitCrudPagination source, boolean fromClient, @EventData("event.detail.newPage") int newPage, @EventData("event.detail.oldPage") int oldPage) {
            super(source, fromClient);
            this.newPage = newPage;
            this.oldPage = oldPage;
        }

        public int getNewPage() {
            return this.newPage;
        }

        public int getOldPage() {
            return this.oldPage;
        }
    }
}

