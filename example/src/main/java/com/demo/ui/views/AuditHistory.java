package com.demo.ui.views;

import com.demo.auditing.model.CustomRevisionEntity;
import com.demo.auditing.service.AuditManager;
import com.demo.ui.MainLayout;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;

import java.util.*;
import java.util.stream.Collectors;

@Route(value = "history", layout = MainLayout.class)
public class AuditHistory extends VerticalLayout implements HasUrlParameter<String> {
    private AuditManager auditManager;
    @Override
    public void setParameter(BeforeEvent event, String revesionId) {
        showHistory(revesionId);
    }

    AuditHistory(AuditManager auditManager){
        this.auditManager = auditManager;

    }

    private void showHistory(String revesionId) {
        int revNumber = Integer.parseInt(revesionId);
        CustomRevisionEntity customRevisionEntity = auditManager.findById(revNumber);
        Set<String> entities = customRevisionEntity.getModifiedEntityNames();

        if(entities.size() < 1)
            return;
        String entityName = entities.stream().findFirst().get();

        try {
            List<Map<String, Object>> history = auditManager.getRevisionHistory(revNumber, entityName);
            String str[] = entityName.trim().split("\\.");
            if(str.length > 0) entityName = str[str.length - 1];
            String tableTitle = entityName.replace("/([A-Z])/g", " $1").trim() + " Revision (" +history.size() + ")";
            Label label = new Label(tableTitle);
            Grid<Map<String, Object>> grid = new Grid<>();
            if(history.size() > 0){
                List<String> columnKeys = history.get(0).keySet().stream().collect(Collectors.toList());
                columnKeys.remove("modifiedBy");
                columnKeys.add(0,"modifiedBy");
                columnKeys.remove("action");
                columnKeys.add(0,"action");

                List<Grid.Column<Map<String, Object>>> columns = new ArrayList<>();
                columnKeys.forEach(columnHeader -> {

                            if(columnHeader.equalsIgnoreCase("id"))
                                return;

                            Grid.Column<Map<String, Object>> column = grid.addColumn(h -> h.get(columnHeader))
                                    .setWidth("12vw").setResizable(true).setHeader(columnHeader.toUpperCase());
                            columns.add(column);
                        }
                );
                grid.setColumnOrder(columns);
                grid.setItems(history);
            }
            Button closeBtn = new Button("Back to Audits");
            closeBtn.addClickListener(e-> UI.getCurrent().navigate(AuditLogView.class));
            HorizontalLayout headerLayout = new HorizontalLayout();
            headerLayout.add(closeBtn);
            headerLayout.setWidthFull();
//            headerLayout.setJustifyContentMode(JustifyContentMode.END);
            add(headerLayout, label, grid);

        }catch (Exception ex){

            ex.printStackTrace();
        }
    }
}
