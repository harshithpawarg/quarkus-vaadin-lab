package com.demo.ui.views;

import com.demo.common.model.Role;
import com.demo.common.model.User;
import com.demo.common.model.UserProfile;
import com.demo.common.service.KeycloakService;
import com.demo.common.service.RoleManager;
import com.demo.common.service.UserManager;
import com.demo.common.service.UserProfileManager;
import com.demo.ui.MainLayout;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.crud.*;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.ColumnTextAlign;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.Column;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.grid.HeaderRow;
import com.vaadin.flow.component.grid.dnd.GridDropLocation;
import com.vaadin.flow.component.grid.dnd.GridDropMode;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.converter.LocalDateToDateConverter;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.data.validator.EmailValidator;
import com.vaadin.flow.data.validator.RegexpValidator;
import com.vaadin.flow.data.validator.StringLengthValidator;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.StreamResource;
import org.apache.commons.lang3.StringUtils;
import org.vaadin.gatanaso.MultiselectComboBox;
import org.vaadin.haijian.Exporter;

import java.time.LocalDate;
import java.time.chrono.ChronoLocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Route(value = "users_role", layout = MainLayout.class)
@CssImport(value = "./styles/gridstyles.css", themeFor = "vaadin-grid")
public class UserWithRoleView  extends VerticalLayout {
    Map<String, Object> selectedColumns;

    List<HashMap<String, Object>> columnOrder;

    private KeycloakService keycloakService;

    private UserProfileManager userProfileManager;

    private RoleManager roleManager;

    private UserManager userManager;

    HashMap<String, Object> draggedItem;

    private final String FIRST_NAME = getTranslation("user.firstName");
    private final String LAST_NAME = getTranslation("user.lastName");
    private final String FULL_NAME = getTranslation("user.fullName");
    private final String USER_NAME = getTranslation("user.userName");
    private final String MIDDLE_NAME = getTranslation("user.middleName");
    private final String PASSWORD = getTranslation("user.password");
    private final String EMAIL = getTranslation("user.email");
    private final String BIRTH_DATE = getTranslation("user.birthDate");
    private final String IS_OWNER = getTranslation("user.isOwner");
    private final String SOCIAL_SECURITY_NUMBER = getTranslation("user.socialSecurityNumber");
    private final String PHONE_NUMBER = getTranslation("user.phoneNumber");

    private final String FIRST_NAME_REQUIRED = getTranslation("user.firstName.required");
    private final String USER_NAME_REQUIRED = getTranslation("user.userName.required");
    private final String EMAIL_REQUIRED = getTranslation("user.email.required");
    private final String LAST_NAME_VALIDATOR = getTranslation("user.lastName.validator");
    private final String USER_NAME_VALIDATOR = getTranslation("user.userName.validator");
    private final String EMAIL_VALIDATOR = getTranslation("user.email.validator");
    private final String BIRTH_DATE_VALIDATOR = getTranslation("user.birthDate.validator");
    private final String SOCIAL_SECURITY_VALIDATOR = getTranslation("user.socialSecurityNumber.validator");
    private final String PHONE_VALIDATOR = getTranslation("user.phoneNumber.validator");

    private final String COLUMN_CHOOSER_OPEN_BTN = getTranslation("user.columnchooser.btn.open");
    private final String COLUMN_CHOOSER_SAVE_BTN = getTranslation("user.columnchooser.btn.save");
    private final String COLUMN_CHOOSER_COLUMN_HEADER = getTranslation("user.columnchooser.cloumnheader");
    private final String COLUMN_CHOOSER_VISIBLE_HEADER = getTranslation("user.columnchooser.visibleheader");
    private final String ROLE_LABEL = getTranslation("role.label");
    private final String FILTER_TEXT = getTranslation("user.filter");
    private final String DOWNLOAD_TEXT = getTranslation("btn.download");
    private final String NEW_USER_BTN = getTranslation("user.btn.newUser");

    public UserWithRoleView(UserManager userManager, UserProfileManager userProfileManager, KeycloakService keycloakService,
                            RoleManager roleManager){
        this.userProfileManager = userProfileManager;
        this.keycloakService = keycloakService;
        this.selectedColumns = getSelectedColumns();
        this.roleManager = roleManager;
        this.userManager = userManager;

        ListDataProvider<User> dataProvider = new ListDataProvider<>(userManager.findAll());
        HorizontalLayout horizontalLayout = new HorizontalLayout();

        Dialog columnChooserDialog = createColumnChooserDialog(dataProvider);
        Button btn = new Button();
        btn.setText(COLUMN_CHOOSER_OPEN_BTN);
        btn.addClickListener(e ->{
            columnChooserDialog.open();
        });

        Crud<User> crud = createUserGrid(dataProvider);
        Anchor csvDownload = createCsvDownloadComponent(dataProvider);
        horizontalLayout.add(btn, csvDownload);
        add(horizontalLayout, crud);
    }

    private Anchor createCsvDownloadComponent(ListDataProvider<User> dataProvider){
        // Instantiate export when button is clicked
        Grid<User> csvGrid = createUserGrid(dataProvider).getGrid();
        csvGrid.removeColumnByKey("vaadin-crud-edit-column");
        Anchor download = new Anchor(new StreamResource("my-excel.xlsx", Exporter.exportAsExcel(csvGrid)), "");
        download.getElement().setAttribute("download", true);
        download.add(new Button(DOWNLOAD_TEXT, new Icon(VaadinIcon.DOWNLOAD_ALT)));
        return download;
    }

    private Dialog createColumnChooserDialog(ListDataProvider<User> dataProvider){
        Dialog dialog = new Dialog();
        Button savebutton = new Button();
        savebutton.setText(COLUMN_CHOOSER_SAVE_BTN);
        dialog.setWidth("30vw");

        List<HashMap<String, Object>> rows = new ArrayList<>();
        Map<String, Object> cols = selectedColumns;
        Set<String> keys = cols.keySet();
        for(String key : keys) {
            HashMap<String, Object> columnBean = new HashMap<>();
            columnBean.put("columnName", key);
            columnBean.put("value", cols.get(key));
            rows.add(columnBean);
        }

        // Create the grid and set its items
        Grid<HashMap<String, Object>> grid = new Grid<>();
        columnOrder = rows;
        grid.setItems(rows);

        grid.addColumn(h -> h.get("columnName")).setHeader(COLUMN_CHOOSER_COLUMN_HEADER);

        grid.setRowsDraggable(true);
        grid.setSelectionMode(SelectionMode.NONE);
        grid.addComponentColumn(item -> {
            Icon icon;
            if((Boolean) item.get("value")){
                icon = VaadinIcon.EYE.create();
                icon.setColor("grey");
            } else {
                icon = VaadinIcon.EYE_SLASH.create();
                icon.setColor("lightgrey");
            }
            icon.addClickListener(e->{
                item.put("value", !(Boolean) item.get("value"));
                updateColumn(item);
                grid.getDataProvider().refreshItem(item);
                grid.getDataProvider().refreshAll();

            });
            return icon;
        })
                .setKey("visible").setTextAlign(ColumnTextAlign.END)
                .setHeader(COLUMN_CHOOSER_VISIBLE_HEADER);

        grid.addDragStartListener(event -> {
            draggedItem = event.getDraggedItems().get(0);
            grid.setDropMode(GridDropMode.BETWEEN);
        });

        grid.addDragEndListener(event -> {
            draggedItem = null;
            grid.setDropMode(null);
        });
        grid.addDropListener(event -> {
            HashMap<String, Object> dropOverItem = event.getDropTargetItem().get();
            if (!dropOverItem.equals(draggedItem)) {
                rows.remove(draggedItem);
                int dropIndex = rows.indexOf(dropOverItem)
                        + (event.getDropLocation() == GridDropLocation.BELOW ? 1
                        : 0);
                rows.add(dropIndex, draggedItem);
                columnOrder = rows;
                grid.getDataProvider().refreshAll();
            }
        });

        savebutton.addClickListener(e->{
            dialog.close();
            refreshGrid(dataProvider);
            updateUserProfile();
        });
        dialog.add(grid, savebutton);

        return dialog;
    }


    private void updateColumn(HashMap<String, Object> column){
        column.get("columnName");
        column.get("value");
        selectedColumns.put((String)column.get("columnName"), column.get("value"));
    }

    private void refreshGrid(ListDataProvider<User> dataProvider){
        Crud<User> currCrud = (Crud<User>) this.getComponentAt(1);
        HorizontalLayout horizontalLayout = (HorizontalLayout) this.getComponentAt(0);
        dataProvider.refreshAll();
        dataProvider.clearFilters();
        ListDataProvider<User> newDataProvider = new ListDataProvider<>(userManager.findAll());
        Crud<User> newCrud = createUserGrid(newDataProvider);
        HorizontalLayout newHorizontalLayout = new HorizontalLayout();
        Anchor csvDownload = createCsvDownloadComponent(newDataProvider);
        newHorizontalLayout.add(horizontalLayout.getComponentAt(0), csvDownload);
        this.remove(horizontalLayout, currCrud);
        this.add(newHorizontalLayout, newCrud);
    }

    private void updateUserProfile() {
        String email = keycloakService.getCurrentUserEmail();
        Map<String, Object> entity = new HashMap<>();
        Map<String, Object> columns = new HashMap<>();
        columns.put("columnChooser", selectedColumns);
        entity.put("userList", columns);
        userProfileManager.createOrUpdateUserProfile(email, entity);
    }

    public Crud<User>  createUserGrid(ListDataProvider<User> dataProvider){
        Grid<User> userGrid = new Grid<>(User.class);
        Crud<User> crud = new Crud<>(User.class, userGrid, createCustomerEditor());
        crud.getGrid().removeAllColumns();
        Set<String> columnList = selectedColumns.keySet();
        crud.getGrid().setColumns((String[]) columnList.toArray(new String[0]));
        Crud.addEditColumn(userGrid);
        List<Column<User>> columns = new ArrayList<>();
        for (HashMap order : columnOrder) {
            Column column;
            if(order.get("columnName").equals("roles")){
                column = crud.getGrid().addColumn(user->getRoleName(user.getRoles()))
                        .setHeader(((String) order.get("columnName")).toUpperCase())
                        .setKey("custom-role")
                        .setSortable(true);
                crud.getGrid().removeColumnByKey("roles");
            }else{
                column = crud.getGrid().getColumnByKey((String) order.get("columnName"))
                        .setHeader(((String) order.get("columnName")).toUpperCase()).setSortable(true);
            }
            column.setVisible((Boolean) order.get("value"));
            columns.add(column);
        }
        columns.add(crud.getGrid().getColumnByKey("vaadin-crud-edit-column"));
        crud.getGrid().setColumnOrder(columns);
        userGrid.setClassNameGenerator(user -> {
            if (user.getIsOwner()) {
                return "active-user";
            } else {
                return "inactive-user";
            }
        });

        createFilters(dataProvider, userGrid);

        CrudI18n i18n = CrudI18n.createDefault();
        i18n.setNewItem(NEW_USER_BTN);

        crud.setI18n(i18n);
        crud.setDataProvider(dataProvider);
        crud.addSaveListener(e -> {
            userManager.save(e.getItem());
            refreshGrid(dataProvider);
        });
        crud.addDeleteListener(e -> {
            userManager.delete(e.getItem());
            refreshGrid(dataProvider);
        });
        crud.addThemeVariants(CrudVariant.NO_BORDER);
        return crud;
    }

    private void createFilters(ListDataProvider<User> dataProvider, Grid<User> userGrid){
        HeaderRow filterRow = userGrid.appendHeaderRow();

        //First Name Filter
        TextField firstNameField = new TextField();
        firstNameField.addValueChangeListener(event -> dataProvider.addFilter(
                user -> StringUtils.containsIgnoreCase(user.getFirstName(),
                        firstNameField.getValue())));
        firstNameField.setValueChangeMode(ValueChangeMode.EAGER);
        filterRow.getCell(userGrid.getColumnByKey("firstName")).setComponent(firstNameField);
        firstNameField.setSizeFull();
        firstNameField.setPlaceholder(FILTER_TEXT);


        //Last Name Filter
        TextField lastNameField = new TextField();
        lastNameField.addValueChangeListener(event -> dataProvider.addFilter(
                user -> StringUtils.containsIgnoreCase(user.getLastName(),
                        lastNameField.getValue())));
        lastNameField.setValueChangeMode(ValueChangeMode.EAGER);
        filterRow.getCell(userGrid.getColumnByKey("lastName")).setComponent(lastNameField);
        lastNameField.setSizeFull();
        lastNameField.setPlaceholder(FILTER_TEXT);


        //Email Filter
        TextField emailField = new TextField();
        emailField.addValueChangeListener(event -> dataProvider.addFilter(
                user -> StringUtils.containsIgnoreCase(user.getEmail(),
                        emailField.getValue())));
        emailField.setValueChangeMode(ValueChangeMode.EAGER);
        filterRow.getCell(userGrid.getColumnByKey("email")).setComponent(emailField);
        emailField.setSizeFull();
        emailField.setPlaceholder(FILTER_TEXT);


        //DOB Filter
        TextField birthDateField = new TextField();
        birthDateField.addValueChangeListener(event -> dataProvider.addFilter(
                user ->  StringUtils.containsIgnoreCase(dateToString(user.getBirthDate()),
                        birthDateField.getValue())));
        birthDateField.setValueChangeMode(ValueChangeMode.EAGER);
        filterRow.getCell(userGrid.getColumnByKey("birthDate")).setComponent(birthDateField);
        birthDateField.setSizeFull();
        birthDateField.setPlaceholder(FILTER_TEXT);


        //Phone Number Filter
        TextField phoneNumberField = new TextField();
        phoneNumberField.addValueChangeListener(event -> dataProvider.addFilter(
                user -> StringUtils.containsIgnoreCase(user.getPhoneNumber(),
                        phoneNumberField.getValue())));
        phoneNumberField.setValueChangeMode(ValueChangeMode.EAGER);
        filterRow.getCell(userGrid.getColumnByKey("phoneNumber")).setComponent(phoneNumberField);
        phoneNumberField.setSizeFull();
        phoneNumberField.setPlaceholder(FILTER_TEXT);

        //Is Owner Filter
        TextField isOwnerField = new TextField();
        isOwnerField.addValueChangeListener(event -> dataProvider.addFilter(
                user -> StringUtils.containsIgnoreCase(convertBooleanToString(user.getIsOwner()),
                        isOwnerField.getValue())));
        isOwnerField.setValueChangeMode(ValueChangeMode.EAGER);
        filterRow.getCell(userGrid.getColumnByKey("isOwner")).setComponent(isOwnerField);
        isOwnerField.setSizeFull();
        isOwnerField.setPlaceholder(FILTER_TEXT);

        //Social Security Number Filter
        TextField socialSecurityNumberField = new TextField();
        socialSecurityNumberField.addValueChangeListener(event -> dataProvider.addFilter(
                user -> StringUtils.containsIgnoreCase(user.getSocialSecurityNumber(),
                        socialSecurityNumberField.getValue())));
        socialSecurityNumberField.setValueChangeMode(ValueChangeMode.EAGER);
        filterRow.getCell(userGrid.getColumnByKey("socialSecurityNumber")).setComponent(socialSecurityNumberField);
        socialSecurityNumberField.setSizeFull();
        socialSecurityNumberField.setPlaceholder(FILTER_TEXT);

        //Role Filter
        Set<Role> roleSet = roleManager.findAll().stream().collect(Collectors.toSet());
        MultiselectComboBox<Role> multiselectComboBox = new MultiselectComboBox<>();
        multiselectComboBox.setItems(roleSet);
        multiselectComboBox.setItemLabelGenerator(role -> role.getName());
        multiselectComboBox.addSelectionListener(e -> {dataProvider.addFilter(
                user -> {
                    Boolean result = false;
                    Set<Role> selectedRoles = multiselectComboBox.getValue();
                    if(selectedRoles.isEmpty()) return true;
                    List<Role> userRoles = user.getRoles();
                    // Displaying the values after iterating through the iterator
                    for (Role selectedRole : selectedRoles) {
                        result = userRoles.stream().anyMatch(role -> role.getName().equals(selectedRole.getName()));
                        if(result) break;
                    }
                    return result;
                });
        });
        multiselectComboBox.setSizeFull();
        multiselectComboBox.setPlaceholder(FILTER_TEXT);
        Column roleColumn = userGrid.getColumnByKey("custom-role");
        if(roleColumn != null) filterRow.getCell(roleColumn).setComponent(multiselectComboBox);
    }

    private String convertBooleanToString(Boolean value){
        return (value != null && value)? "true" : "false";
    }

    private String dateToString(Date date){
        return date != null ? date.toString() : "";
    }

    private Map<String, Object> getSelectedColumns(){
        String email = keycloakService.getCurrentUserEmail();
        UserProfile userProfile = userProfileManager.getUserProfileByName(email);
        Map<String, Object> columns = getDefaultColumns();
        if(userProfile != null && userProfile.getInfo() != null
                && userProfile.getInfo().get("userList") != null) {
            Map<String, Object> userListDetails = (Map<String, Object>) userProfile.getInfo().get("userList");
            if (userListDetails.get("columnChooser") != null) {
                columns = (Map<String, Object>) userListDetails.get("columnChooser");
            }
        }
        return columns;
    }

    private Map<String, Object> getDefaultColumns(){
        Map<String, Object> cols = new HashMap<>();

        cols.put("firstName", true);
        cols.put("lastName",  true);
        cols.put("email", true);
        cols.put("birthDate", true);
        cols.put("phoneNumber", true);
        cols.put("isOwner", true);
        cols.put("socialSecurityNumber", true);
        cols.put("roles", true);
        return cols;
    }

    private CrudEditor<User> createCustomerEditor() {
        TextField firstName = new TextField(FIRST_NAME);
        TextField middleName = new TextField(MIDDLE_NAME);
        TextField lastName = new TextField(LAST_NAME);
        TextField fullName = new TextField(FULL_NAME);
        TextField userName = new TextField(USER_NAME);
        // PasswordField password = new PasswordField("password");
        EmailField email = new EmailField(EMAIL);
        Checkbox isOwner = new Checkbox(IS_OWNER);
        DatePicker birthDate = new DatePicker(BIRTH_DATE);
        List<Role> roleList = roleManager.findAll();
        Set<Role> roleSet = roleList.stream().collect(Collectors.toSet());
        MultiselectComboBox<Role> multiselectComboBox = new MultiselectComboBox<>();
        multiselectComboBox.setItems(roleSet);
        multiselectComboBox.setLabel(ROLE_LABEL);
        multiselectComboBox.setItemLabelGenerator(role -> role.getName());

        TextField socialSecurityNumber = new TextField(SOCIAL_SECURITY_NUMBER);
        TextField phoneNumber = new TextField(PHONE_NUMBER);

        FormLayout form = new FormLayout(firstName, middleName, lastName, fullName, userName,
                email,phoneNumber,socialSecurityNumber,multiselectComboBox,birthDate,isOwner);

        Binder<User> binder = new Binder<>(User.class);

        ChronoLocalDate dt
                = LocalDate.of(2000,01,01);
        binder.forField(birthDate)
                .withValidator(brdate ->
                        (brdate == null || brdate.isBefore(dt)), BIRTH_DATE_VALIDATOR)
                .withConverter(new LocalDateToDateConverter())
                .bind(User::getBirthDate, User::setBirthDate);
        binder.forField(firstName)
                .asRequired(FIRST_NAME_REQUIRED)
                .bind(User::getFirstName, User::setFirstName);
        binder.forField(lastName)
                .withValidator(ln -> (ln == null || ln.isEmpty()|| ln.length() > 2),LAST_NAME_VALIDATOR)
                .bind(User::getLastName, User::setLastName);
        binder.forField(middleName)
                .bind(User::getMiddleName, User::setMiddleName);
        binder.forField(fullName)
                .bind(User::getFullName, User::setFullName);
        binder.forField(userName)
                .asRequired(USER_NAME_REQUIRED)
                .withValidator(new StringLengthValidator(USER_NAME_VALIDATOR, 3, 20))
                .bind(User::getUserName, User::setUserName);

        binder.forField(email)
                .asRequired(EMAIL_REQUIRED)
                .withValidator(new EmailValidator(EMAIL_VALIDATOR)).bind(User::getEmail, User::setEmail);
        binder.bind(isOwner, User::getIsOwner, User::setIsOwner);
        binder.forField(multiselectComboBox).bind(user -> user.getRoles().stream().collect(Collectors.toSet()),((user, roles) -> {user.setRoles(roles.stream().collect(Collectors.toList()));}));

        binder.forField(socialSecurityNumber)
                .withValidator(new RegexpValidator(SOCIAL_SECURITY_VALIDATOR, "^\\s*|(?!000|666)[0-8][0-9]{2}-(?!00)[0-9]{2}-(?!0000)[0-9]{4}$"))
                .bind(User::getSocialSecurityNumber, User::setSocialSecurityNumber);
        String phoneNumberRegex = "\\s*|\\(\\d{3}\\)-\\d{3}-\\d{4}";
        binder.forField(phoneNumber)
                .withValidator(new RegexpValidator(PHONE_VALIDATOR , phoneNumberRegex))
                .bind(User::getPhoneNumber, User::setPhoneNumber);

        return new BinderCrudEditor<>(binder, form);
    }

    public Set<Role> getRoles(User user){
        Set<Role> roleSet = new HashSet<>();
        if(user != null && user.getRoles() != null)
            roleSet = user.getRoles().stream().collect(Collectors.toSet());
        return roleSet;
    }

    private String getRoleName(List<Role> rolesSet){

        StringBuilder roles = new StringBuilder("");
        rolesSet.forEach(role -> {
            if (roles.length()>0) roles.append(",");
            roles.append(role.getName());
        });
        return roles.toString();
    }
}
