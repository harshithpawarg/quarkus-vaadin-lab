package com.demo.ui.views;

import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public class Card extends VerticalLayout {
    private final Div titleDiv;
    private final Div contentDiv;

    /**
     * Creates the hello world template.
     */
    public Card(String title, String content) {
        setClassName("party-counter-card");
        setWidthFull();

        titleDiv = new Div();
        titleDiv.setWidthFull();
        titleDiv.setText(title);
        add(titleDiv);

        contentDiv = new Div();
        contentDiv.setWidthFull();
        contentDiv.setText(content);
        add(contentDiv);
    }
    public Card() {
        this("","");
    }

    public void setTitle(String title){
        titleDiv.setText(title);
    }


    public void setContent(String content){
        contentDiv.setText(content);
    }
}