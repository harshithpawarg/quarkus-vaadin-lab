package com.demo.ui.views;

import com.demo.common.service.UserManager;
import com.demo.ui.MainLayout;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinSession;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.util.Collection;

@Route(value = "dashboard", layout = MainLayout.class)
public class DashboardView extends VerticalLayout {
    Label sessionCount = new Label("");
    Label sessionSize = new Label("");

    DashboardView(UserManager userManager) {
        //writeSession(session);
        Button refreshBtn = new Button("Refresh");
        //refreshBtn.addClickListener(e-> writeSession(session));
        add(sessionCount, sessionSize, refreshBtn);
    }

    public void writeSession(HttpSession httpSession) {
        try {
            // use buffering
            Collection<VaadinSession> sessions = VaadinSession.getAllSessions(httpSession);
            sessionCount.setText("Total sessions = "+sessions.size());

            OutputStream file = new FileOutputStream("session.ser");
            OutputStream buffer = new BufferedOutputStream(file);
            ObjectOutput output = new ObjectOutputStream(buffer);
            try {
                // 'this' is the application class instance
                output.writeObject(sessions);
            } finally {
                output.close();

                File sessionFile = new File("session.ser");
                if (!sessionFile.exists() || !sessionFile.isFile()) return;

                sessionSize.setText("Session size = "+ ((double) sessionFile.length() / 1024) + "  kb");
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
