package com.demo.ui.views;

import com.demo.ui.MainLayout;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

@Route(value = "timelines", layout = MainLayout.class)
public class TimelineView  extends VerticalLayout {

    public TimelineView(){
        Label label = new Label("Timeline");
        add(label);
    }
}
