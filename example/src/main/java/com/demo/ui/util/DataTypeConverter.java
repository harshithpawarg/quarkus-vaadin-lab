package com.demo.ui.util;

public class DataTypeConverter {

    public static boolean ConvertStringToBool(String filterValue){
        if(filterValue.equalsIgnoreCase("YES")) return true;
        else return false;
    }

    public static String ConvertBoolToString(Boolean filterValue){
        if(filterValue) return "Yes";
        else return "No";
    }
}
