package com.demo.ui.components.navigation.drawer;

//import com.demo.common.service.AutoWireHelper;
import com.demo.common.service.KeycloakService;
import com.demo.ui.util.UIUtils;
import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.ClientCallable;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.AfterNavigationObserver;
import elemental.json.JsonObject;
import org.keycloak.representations.AccessToken;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

@CssImport("./styles/components/navi-drawer.css")
@JsModule("./js/swipe-away.js")
public class NaviDrawer extends Div
		implements AfterNavigationObserver {

	@Autowired
	private KeycloakService keycloakService;

	private String CLASS_NAME = "navi-drawer";
	private String RAIL = "rail";
	private String OPEN = "open";

	private Div scrim;

	private Div mainContent;
	private TextField search;

	private Button railButton;
	private NaviMenu menu;

	@Override
	protected void onAttach(AttachEvent attachEvent) {
		super.onAttach(attachEvent);
		UI ui = attachEvent.getUI();
		ui.getPage().executeJavaScript("window.addSwipeAway($0,$1,$2,$3)",
				mainContent.getElement(), this, "onSwipeAway",
				scrim.getElement());
	}

	@ClientCallable
	public void onSwipeAway(JsonObject data) {
		close();
	}

	public NaviDrawer(KeycloakService keycloakService) {
		this.keycloakService = keycloakService;
//		AutoWireHelper.autowire(this, this.keycloakService);
//		this.keycloakService = keycloakService;
		setClassName(CLASS_NAME);

		initScrim();
		initMainContent();

		initHeader();
		initUserDetails();
//		initSearch();

		initMenu();

		initFooter();
	}

	private void initScrim() {
		// Backdrop on small viewports
		scrim = new Div();
		scrim.addClassName(CLASS_NAME + "__scrim");
		scrim.addClickListener(event -> close());
		add(scrim);
	}

	private void initMainContent() {
		mainContent = new Div();
		mainContent.addClassName(CLASS_NAME + "__content");
		add(mainContent);
	}

	private void initHeader() {
		System.out.println(keycloakService);
		String tenantName = getTenantName().toUpperCase();
		mainContent.add(new BrandExpression(tenantName));
	}

	private void initUserDetails(){
		String userName = getUserName();
		String tenantName = getTenantName();
		List tenants = getTenantList();
		VerticalLayout paymenLayout = new VerticalLayout();
		String nameAndTenant = userName +"@"+tenantName;
		Span span = new Span(nameAndTenant);
		span.addClassName("brand-expression__title");
		ComboBox<String> tenantsComboBox = new ComboBox<>();
		tenantsComboBox.setAllowCustomValue(false);
		tenantsComboBox.setItems(tenants);
		tenantsComboBox.setValue(nameAndTenant);
		tenantsComboBox.addValueChangeListener(event -> {
			System.out.println("tenant changed ...");
			changeTenant(event.getValue());
		});
		tenantsComboBox.addClassName("brand-expression__title");
		paymenLayout.add(tenantsComboBox);
		paymenLayout.setClassName("brand-expression__name");
		mainContent.add(paymenLayout);
	}

	private void initSearch() {
		search = new TextField();
		search.addValueChangeListener(e -> menu.filter(search.getValue()));
		search.setClearButtonVisible(true);
		search.setPlaceholder("Search");
		search.setPrefixComponent(new Icon(VaadinIcon.SEARCH));
		mainContent.add(search);
	}

	private void initMenu() {
		menu = new NaviMenu();
		mainContent.add(menu);
	}

	private void initFooter() {
		railButton = UIUtils.createSmallButton("Collapse", VaadinIcon.CHEVRON_LEFT_SMALL);
		railButton.addClassName(CLASS_NAME + "__footer");
		railButton.addClickListener(event -> toggleRailMode());
		railButton.getElement().setAttribute("aria-label", "Collapse menu");
		mainContent.add(railButton);
	}

	private void toggleRailMode() {
		if (getElement().hasAttribute(RAIL)) {
			getElement().setAttribute(RAIL, false);
			railButton.setIcon(new Icon(VaadinIcon.CHEVRON_LEFT_SMALL));
			railButton.setText("Collapse");
			UIUtils.setAriaLabel("Collapse menu", railButton);

		} else {
			getElement().setAttribute(RAIL, true);
			railButton.setIcon(new Icon(VaadinIcon.CHEVRON_RIGHT_SMALL));
			railButton.setText("Expand");
			UIUtils.setAriaLabel("Expand menu", railButton);
			getUI().get().getPage().executeJavaScript(
					"var originalStyle = getComputedStyle($0).pointerEvents;" //
							+ "$0.style.pointerEvents='none';" //
							+ "setTimeout(function() {$0.style.pointerEvents=originalStyle;}, 170);",
					getElement());
		}
	}

	public void toggle() {
		if (getElement().hasAttribute(OPEN)) {
			close();
		} else {
			open();
		}
	}

	private void open() {
		getElement().setAttribute(OPEN, true);
	}

	private void close() {
		getElement().setAttribute(OPEN, false);
		applyIOS122Workaround();
	}

	private void applyIOS122Workaround() {
		// iOS 12.2 sometimes fails to animate the menu away.
		// It should be gone after 240ms
		// This will make sure it disappears even when the browser fails.
		getUI().get().getPage().executeJavaScript(
				"var originalStyle = getComputedStyle($0).transitionProperty;" //
						+ "setTimeout(function() {$0.style.transitionProperty='padding'; requestAnimationFrame(function() {$0.style.transitionProperty=originalStyle})}, 250);",
				mainContent.getElement());
	}

	public NaviMenu getMenu() {
		return menu;
	}

	private String getUserName() {
		String userName = keycloakService.getCurrentUserEmail();
		if (userName == null)
			userName = "Demo User";
		return userName;
	}

	private String getTenantName() {
		String tenantName = keycloakService.getCurrentTenant();
		System.out.println("AFTER REFRESHING : tenant ="+keycloakService.getCurrentTenant());
		if (tenantName == null)
			tenantName = "Demo";
		return tenantName;
	}

	private List getTenantList() {
		Map<String, Object> claims = keycloakService.getOtherClaims();
		return (List) claims.get("tenant");
	}

	private void changeTenant(String tenant) {
		String email =  keycloakService.getCurrentUserEmail();
		System.out.println(email);
		boolean updated = keycloakService.updateCurrentTenant(email, tenant);
		if(updated) {
			System.out.println("BEFORE REFRESHING : tenant ="+keycloakService.getCurrentTenant());
			UI.getCurrent().getPage().reload();
		}
	}

	@Override
	public void afterNavigation(AfterNavigationEvent afterNavigationEvent) {
		close();
	}

}
