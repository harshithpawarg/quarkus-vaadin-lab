//package com.demo.common.exception;
//
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.ControllerAdvice;
//import org.springframework.web.bind.annotation.ExceptionHandler;
//import org.springframework.web.context.request.WebRequest;
//import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
//import com.bugsnag.Bugsnag;
//
//import java.util.ArrayList;
//import java.util.List;
//
//@ControllerAdvice
//public class GlobalExceptionHandler extends ResponseEntityExceptionHandler
//{
//
//    @Autowired
//    Bugsnag bugsnag;
//
//    @ExceptionHandler(Exception.class)
//    public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
//
//        System.out.println("######## GLOBAL EXCEPTION ########");
//        List<String> details = new ArrayList<>();
//        details.add(ex.getLocalizedMessage());
//        ex.printStackTrace();
//        ErrorResponse error = new ErrorResponse("Server Error", details);
//        bugsnag.notify(ex);
//        return new ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
//    }
//}
//
