//package com.demo.common.exception;
//
//import com.bugsnag.Bugsnag;
//import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
//import org.springframework.beans.factory.annotation.Autowired;
//
//import java.lang.reflect.Method;
//
//public class CustomAsyncExceptionHandler implements AsyncUncaughtExceptionHandler {
//
//    @Autowired
//    Bugsnag bugsnag;
//
//
//    @Override
//    public void handleUncaughtException(final Throwable ex, final Method method, final Object... obj) {
//        ex.printStackTrace();
//        bugsnag.notify(ex);
//    }
//}
