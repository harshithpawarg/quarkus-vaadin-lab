package com.demo.common.notifications;

import com.demo.common.model.Party;
import com.vaadin.flow.shared.Registration;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

public class Broadcaster {

    static Executor executor = Executors.newSingleThreadExecutor();

    static LinkedList<Consumer<List<Party>>> listeners = new LinkedList<>();

    public static synchronized Registration register(
            Consumer<List<Party>> listener) {
        listeners.add(listener);

        return () -> {
            synchronized (Broadcaster.class) {
                listeners.remove(listener);
            }
        };
    }

    public static synchronized void broadcast(List<Party> partyList) {
        for (Consumer<List<Party>> listener : listeners) {
            executor.execute(() -> listener.accept(partyList));
        }
    }

}
