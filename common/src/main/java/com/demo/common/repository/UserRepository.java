package com.demo.common.repository;

import com.demo.common.model.Role;
import com.demo.common.model.Tenant;
import com.demo.common.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.enterprise.context.RequestScoped;
import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

//    List<User> findByEmail(String email);

    User findByEmail(String email);

    List<User> findByIdIn(List<Long> userIds);

    List<User> findByFullName(String fullName);
}
