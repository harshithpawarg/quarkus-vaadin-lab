package com.demo.common.repository;

import com.demo.common.model.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface TenantRepository extends JpaRepository<Tenant, Long> {

//    @Override
//    @Query(nativeQuery = true, value = "SELECT * FROM tenants ORDER BY id ASC")
//    List<Tenant> findAll();

}

