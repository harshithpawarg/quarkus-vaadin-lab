package com.demo.common.repository;

import com.demo.common.model.Report;
import com.demo.common.model.ReportAttribute;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ReportAttributeRepository extends JpaRepository<ReportAttribute, Long> {
//    List<ReportAttribute> findByReportAndNameIgnoreCaseContaining(Report report, String name);
    List<ReportAttribute> findByReport(Report report);
}
