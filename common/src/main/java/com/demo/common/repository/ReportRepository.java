package com.demo.common.repository;

import com.demo.common.model.Report;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ReportRepository extends JpaRepository<Report, Long> {
    List<Report> findByPermissionContainingIgnoreCase(String permission);
}
