package com.demo.common.repository;

import com.demo.common.model.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
//import org.springframework.transaction.annotation.Transactional;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface UserRoleRepository extends JpaRepository<UserRole, Long> {
    UserRole findByUserId(Long userId);

    @Transactional
    void deleteByUserId(Long userId);

    UserRole findByUserIdAndRoleId(Long userId, Long roleId);
}
