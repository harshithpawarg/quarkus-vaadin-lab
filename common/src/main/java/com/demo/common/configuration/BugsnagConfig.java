//package com.demo.common.configuration;
//
//import com.bugsnag.Bugsnag;
//import com.bugsnag.BugsnagSpringConfiguration;
//import com.demo.common.model.User;
//import com.demo.common.repository.UserRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Import;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.context.SecurityContextHolder;
//
//@Configuration
//@Import(BugsnagSpringConfiguration.class)
//public class BugsnagConfig {
//
//    private static final String PRODUCTION_ENVIRONMENT = "PRODUCTION";
//    private static final String STAGING_ENVIRONMENT = "STAGING";
//
//    @Value("${spring.bugsnag.apiKey}")
//    private String apiKey;
//
//    @Value("${spring.releaseStage.environment}")
//    private String environment;
//
//    @Value("${spring.version}")
//    private String version;
//
//    @Autowired
//    private UserRepository userRepository;
//
//    @Bean
//    public Bugsnag bugsnag() {
//        Bugsnag bugsnag =  new Bugsnag(apiKey);
//        bugsnag.setReleaseStage(environment);
//        bugsnag.setAutoCaptureSessions(true);
//        bugsnag.setAppVersion(version);
//        bugsnag.addCallback(report -> {
//            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//            String currentPrincipalName = null;
//            if(authentication != null) {
//                currentPrincipalName = authentication.getName();
//                User user = userRepository.findByEmailIgnoreCase(currentPrincipalName);
//                String tenantName = null;
//                if (user != null) {
////                    tenantName = user.getTenant() != null ? user.getTenant().getName() : null;
//                    report.setUserId(user.getId().toString());
//                    report.setUserName(user.getFullName());
//                    report.setUserEmail(currentPrincipalName);
//                    report.addToTab("Tenant", "tenant", tenantName);
//                }
//                String exceptionPrefix = "";
//                if(environment.equalsIgnoreCase(STAGING_ENVIRONMENT))
//                    exceptionPrefix = exceptionPrefix.concat("ST:");
//                else if(environment.equalsIgnoreCase(PRODUCTION_ENVIRONMENT))
//                    exceptionPrefix = exceptionPrefix.concat("PD:");
//                else
//                    exceptionPrefix = exceptionPrefix.concat("DEV:");
//
//                exceptionPrefix = exceptionPrefix.concat(tenantName != null ? tenantName.toUpperCase() : "") + " ";
//                report.setExceptionName(exceptionPrefix + " " + report.getExceptionName());
//            }
//            report.getException().printStackTrace();
//            System.out.println("Bugsnag: Error reported to bugsnag. User: " + currentPrincipalName);
//        });
//
//        return bugsnag;
//    }
//}
