//package com.demo.common.configuration;
//
//import com.demo.common.exception.CustomAsyncExceptionHandler;
//import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.task.SyncTaskExecutor;
//import org.springframework.scheduling.annotation.AsyncConfigurerSupport;
//import org.springframework.scheduling.annotation.EnableAsync;
//import org.springframework.security.concurrent.DelegatingSecurityContextExecutorService;
//
//import java.util.concurrent.Executor;
//import java.util.concurrent.Executors;
//
//@Configuration
//@EnableAsync
//public class AsyncConfiguration extends AsyncConfigurerSupport {
//
//    @Override
//    public Executor getAsyncExecutor() {
//        if(!ApplnContext.getIsMigrationRunning()){
//            return new DelegatingSecurityContextExecutorService(Executors.newFixedThreadPool(5));
//        }
//        else {
//            return new SyncTaskExecutor();
//        }
//
//    }
//
//    @Override
//    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
//        return new CustomAsyncExceptionHandler();
//    }
//
//}
