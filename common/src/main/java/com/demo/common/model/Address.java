package com.demo.common.model;


import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.Audited;
import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "addresses")
@Audited(withModifiedFlag = true)
public class Address extends AuditModel {

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "address_Sequence")
    @SequenceGenerator(name = "address_Sequence", sequenceName = "ADDRESS_SEQUENCE", allocationSize = 1)
    private Long id;

    private String address;

    private String city;

    private String state;

    @Column( name = "zip_code" )
    @Audited(modifiedColumnName = "zip_code_mod")
    private String zipCode;

    public Address() {
    }

}
