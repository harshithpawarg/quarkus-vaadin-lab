package com.demo.common.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.Audited;
import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "user_roles")
@Audited(withModifiedFlag = true)
public class UserRole extends AuditModel {

    @Id
    @SequenceGenerator(name = "userRolesSequence", sequenceName = "user_roles_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "userRolesSequence")

    private Long id;

    @Column(name = "user_id")
    @Audited(modifiedColumnName = "user_id_mod")
    private Long userId;

    @Column(name = "role_id")
    @Audited(modifiedColumnName = "role_id_mod")
    private Long roleId;

    public UserRole() {}

    public UserRole(Long userId, Long roleId) {
        this.userId = userId;
        this.roleId = roleId;
    }

}
