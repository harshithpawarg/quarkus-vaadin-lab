package com.demo.common.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "parties")
@Audited(withModifiedFlag = true)
public class Party extends AuditModel {

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "party_Sequence")
    @SequenceGenerator(name = "party_Sequence", sequenceName = "PARTY_SEQUENCE", allocationSize = 1)
    private Long id;

    @org.hibernate.annotations.Index(name = "parties_name_index")
    private String name;

    private String email;

    @Column( name = "phone_number" )
    @Audited(modifiedColumnName = "phone_number_mod")
    private String phoneNumber;

    @Column( name = "contact_person" )
    @Audited(modifiedColumnName = "contact_person_mod")
    private String contactPerson;

    @Column(name = "is_funder", columnDefinition = "boolean default false")
    @Audited(modifiedColumnName = "is_funder_mod")
    private Boolean isFunder = false;

    @Column( name = "business_legal_name" )
    @Audited(modifiedColumnName = "business_legal_name_mod")
    private String businessLegalName;

    @ManyToOne
    @JoinColumn(name = "physical_address_id")
    @Audited(modifiedColumnName = "physical_address_mod")
    private Address physicalAddress;

    @ManyToOne
    @JoinColumn(name = "mailing_address_id")
    @Audited(modifiedColumnName = "mailing_address_mod")
    private Address mailingAddress;

    @Temporal(value = TemporalType.DATE)
    @Column( name = "business_start_date" )
    @Audited(modifiedColumnName = "business_start_date_mod")
    private Date businessStartDate;

    @Column(name = "enable", columnDefinition = "boolean default true")
    private Boolean enable = true;

    public Party() {

    }
}