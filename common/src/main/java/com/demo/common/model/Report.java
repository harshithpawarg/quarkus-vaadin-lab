package com.demo.common.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "reports")
@Audited(withModifiedFlag = true)
public class Report extends AuditModel {

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator = "report_Sequence")
    @SequenceGenerator(name = "report_Sequence", sequenceName = "report_SEQUENCE")
    private Long id;

    private String name;

    @Column( name = "file_name" )
    @Audited(modifiedColumnName = "file_name_mod")
    private String fileName;

    @Column( name = "file_path" )
    @Audited(modifiedColumnName = "file_path_mod")
    private String filePath;

    private String permission;

    @NotAudited
    @OneToMany(mappedBy = "report", orphanRemoval=true)
    private List<ReportAttribute> reportAttributes = new ArrayList<>(0);
}
