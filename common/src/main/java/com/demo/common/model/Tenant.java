package com.demo.common.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "tenants")
@Audited(withModifiedFlag = true)
public class Tenant extends AuditModel {

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "tenant_Sequence")
    @SequenceGenerator(name = "tenant_Sequence", sequenceName = "TENANT_SEQUENCE", allocationSize = 1)
    private Long id;

    @NotBlank
    private String name;

    @Column(name = "profile_path")
    @Audited(modifiedColumnName = "profile_path_mod")
    private String profilePath;

    @NotAudited
    @OneToMany(mappedBy = "tenant", orphanRemoval=true)
    private List<Role> roles = new ArrayList<>(0);

    public Tenant() {}

}