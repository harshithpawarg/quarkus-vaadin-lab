package com.demo.common.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "roles")
@Audited(withModifiedFlag = true)
public class Role extends AuditModel {

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "role_Sequence")
    @SequenceGenerator(name = "role_Sequence", sequenceName = "ROLE_SEQUENCE", allocationSize = 1)
    private Long id;

    @NotBlank
    @Size(min = 3, max = 15)
    private String name;

    public Role() {}

    @NotAudited
    @ManyToMany(mappedBy = "roles")
    private List<User> users;

    @ManyToOne
    private Tenant tenant;

}
