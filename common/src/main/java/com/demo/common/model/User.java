package com.demo.common.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@Entity
@Table(name = "users")
@Audited(withModifiedFlag = true)
public class User extends AuditModel {
    @Id
    @SequenceGenerator(name = "usersSequence", sequenceName = "users_id_seq", allocationSize = 1, initialValue = 10)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "usersSequence")
    private Long id;

    @Column(name = "first_name")
    @Audited(modifiedColumnName = "first_name_mod")
    private String firstName;

    @Column(name = "last_name")
    @Audited(modifiedColumnName = "last_name_mod")
    private String lastName;

    @Column(name = "full_name")
    @Audited(modifiedColumnName = "full_name_mod")
    private String fullName;

    //@Email(message = "Must be a well-formed email address")
    @Column(name = "user_name")
    @Audited(modifiedColumnName = "user_name_mod")
    private String userName;

    @Size(min = 6, message="Password should have atleast 6 characters")
    private String password;

    private String email;

    @Temporal(value = TemporalType.DATE)
    @Column( name = "birth_date" )
    @Audited(modifiedColumnName = "birth_date_mod")
    private Date birthDate;

    @Column(name = "is_owner", columnDefinition = "boolean default false")
    @Audited(modifiedColumnName = "is_owner_mod")
    private Boolean isOwner = false;

    @Column( name = "social_security_number" )
    @Audited(modifiedColumnName = "social_security_number")
    private String socialSecurityNumber;

    @ManyToOne
    @JoinColumn(name = "tenant_id")
    @Audited(modifiedColumnName = "tenant_mod")
    private Tenant tenant;

    @Column( name = "middle_name" )
    @Audited(modifiedColumnName = "middle_name_mod")
    private String middleName;

    private String title;

    @Column( name = "phone_number" )
    @Audited(modifiedColumnName = "phone_number_mod")
    private String phoneNumber;

    @Column( name = "secure_user_id" )
    @Audited(modifiedColumnName = "secure_user_id_mod")
    private String secureUserId;

    public User() {}

    @NotAudited
    @ManyToMany(cascade=CascadeType.MERGE)
    @JoinTable(
            name="user_roles",
            joinColumns={@JoinColumn(name="user_id", referencedColumnName="id")},
            inverseJoinColumns={@JoinColumn(name="role_id", referencedColumnName="id")})
    private List<Role> roles;


    @OneToOne(cascade =  CascadeType.ALL,
            mappedBy = "user",
            orphanRemoval=true)
    @NotAudited
    @JoinColumn(name = "user_profile_id")
    private UserProfile userProfile;

    @Override
    public String toString() {
       return id + ", " + email + ", " + firstName + ", " + lastName +
       ",  " + userName + ", " + phoneNumber + ", " +socialSecurityNumber;
    }
}