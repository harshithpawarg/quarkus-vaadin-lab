package com.demo.common.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.Audited;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "report_attributes")
@Audited(withModifiedFlag = true)
public class ReportAttribute extends AuditModel {

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator = "report_attribute_Sequence")
    @SequenceGenerator(name = "report_attribute_Sequence", sequenceName = "report_attribute_SEQUENCE")
    private Long id;

    private String name;

    @ManyToOne
    @JoinColumn(name = "report_id")
    private Report report;
}
