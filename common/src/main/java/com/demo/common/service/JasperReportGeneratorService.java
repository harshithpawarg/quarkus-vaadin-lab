package com.demo.common.service;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRCsvDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.io.*;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;


@Component
public class JasperReportGeneratorService {

    @Autowired
    public DataSource dataSource;

    private static final String FOLDER = "../resources/JasperReportGenerator/Documents/";

    public void generatePdfReport(String outputFileName, String reportTemplate, Map<String, Object> map) throws Exception {
            JasperReport jasperReport = JasperCompileManager.compileReport(reportTemplate);
            Connection conn = null;
            try {
                conn = dataSource.getConnection();
                JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, map, conn);
                if (outputFileName.contains("pdf")) {
                    JasperExportManager.exportReportToPdfFile(jasperPrint, outputFileName);
                }
            } finally {
                if (conn != null)
                    conn.close();
            }
    }

    private JRCsvDataSource getDataSource() throws Exception {
        JRCsvDataSource ds = null;
        String csvFile = FOLDER + "letter.csv";
        System.out.println("CSV FILE = " + csvFile);
            BufferedReader reader = new BufferedReader(new FileReader(csvFile));
            String csvHeader = reader.readLine();
            String[] columnNames = csvHeader.split(",");
            ds = new JRCsvDataSource(reader);
            ds.setRecordDelimiter("\r\n");
            ds.setColumnNames(columnNames);
        return ds;
    }



}

