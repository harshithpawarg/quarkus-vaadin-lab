package com.demo.common.service;

import com.demo.common.model.*;
import com.demo.common.repository.UserRepository;
import com.demo.common.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.enterprise.context.RequestScoped;
import java.io.File;
import java.math.BigDecimal;
import java.util.*;

@Component
@RequestScoped
public class UserManager {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private RoleManager roleManager;

    @Value("${spring.version}")
    private String version;

    @Value("${spring.releaseStage.environment}")
    private String environment;

    @Autowired
    private JasperReportGeneratorService jasperReportGeneratorService;

    public Map<String, Object> getUserInfo(String email) {

        Map<String, Object> response = new HashMap<>();
        User user = userRepository.findByEmail(email);
        if (user != null) {

            List<String> roles = new ArrayList<>();
            List<Role> roleList = user.getRoles();
            for(Role role : roleList) {
                roles.add(role.getName());
            }

            Map<String, Object> userSummary = new HashMap<>();
            userSummary.put("id", user.getId());
            userSummary.put("email", user.getEmail());
            userSummary.put("password", user.getPassword());
            userSummary.put("userName", user.getUserName());
            userSummary.put("firstName", user.getFirstName());
            userSummary.put("lastName", user.getLastName());
            userSummary.put("fullName", user.getFullName());
            userSummary.put("roles", roles);

            response.put("userSummary", userSummary);
            response.put("version", version);
            response.put("environment", environment);
        }

        return response;
    }

    public Boolean isAdmin(String email){
        User user = userRepository.findByEmail(email);
        List<Role> roles = user.getRoles();
        return roles.stream().anyMatch(role1 -> role1.getName().equalsIgnoreCase("admin"));
    }

    public List<String> getRolesForEmail(String email){
        User user = userRepository.findByEmail(email);
        List<String> roles = new ArrayList<>();
        List<Role> roleList = user.getRoles();
        for(Role role : roleList) {
            roles.add(role.getName());
        }
        return roles;
    }

    public String generateLetter(Report report, Map<String, Object> attr) throws Exception {
        String uuidToken = UUID.randomUUID().toString();

        String fileName = "report" + "_" + uuidToken + ".pdf";
        String outputFileName = "../resources/jasperReport/" + fileName;

        jasperReportGeneratorService.generatePdfReport(outputFileName, report.getFilePath(), attr);
        return outputFileName;
    }

    public List<User> findAll(){
        return userRepository.findAll();
    }

    public void save(User user){
        System.out.println("user: " + user.toString() + user.getRoles());
        List<Role> roles = user.getRoles();
        user.setRoles(new ArrayList<>());
        user = userRepository.save(user);
        if (roles != null) {
            for (Role role : roles) {
                UserRole userRole = new UserRole(user.getId(), role.getId());
                userRoleRepository.save(userRole);
            }
        }
        User user1 = userRepository.findById(user.getId()).get();
        user.setRoles(user1.getRoles());
        userRepository.save(user);
    }
    public void delete(User user){
        userRepository.delete(user);
    }
}
