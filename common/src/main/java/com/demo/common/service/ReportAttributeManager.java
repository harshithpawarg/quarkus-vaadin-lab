package com.demo.common.service;

import com.demo.common.model.Report;
import com.demo.common.model.ReportAttribute;
import com.demo.common.repository.ReportAttributeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ReportAttributeManager {
    @Autowired
    public ReportAttributeRepository reportAttributeRepository;

    public void create(String name, Report report){
        ReportAttribute reportAttribute = new ReportAttribute();
        reportAttribute.setName(name);
        reportAttribute.setReport(report);
        reportAttributeRepository.save(reportAttribute);
    }

    public void save(ReportAttribute reportAttribute){
        reportAttributeRepository.save(reportAttribute);
    }

    public void delete(ReportAttribute reportAttribute){
        reportAttributeRepository.delete(reportAttribute);
    }

    public List<ReportAttribute> findByReport(Report report){
        return reportAttributeRepository.findByReport(report);
    }

}
