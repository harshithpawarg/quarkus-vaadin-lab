package com.demo.common.service;

import com.demo.common.model.Party;
import com.demo.common.notifications.Broadcaster;
import com.demo.common.repository.PartyRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.control.ActivateRequestContext;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@ActivateRequestContext
public class PartyManager {

    @Autowired
    private PartyRepository partyRepository;

    @Autowired
    private MqttManager mqttManager;

    @Autowired
    private NotificationEventPublisher notificationEventPublisher;

    public void setOnMessageReceived() {
        mqttManager.setOnMessageReceived(event -> {
            TenantContext.setCurrentTenant("demo");
            System.out.println("sssssssssssssssssssss" + event.MESSAGE.toString());
            JSONObject jsonObject = new JSONObject(event.MESSAGE.toString());
            String action = jsonObject.getString("action");
            if (action != null) {
                if (action.equals("fetchAllParties")) {
                    sendPartyDetails();
                } else if (action.equals("partyUpdate")) {
                    savePartyDetails(jsonObject);
                }
            }
        });
    }

    public void savePartyDetails(Party party) {
        save(party);
        //notificationEventPublisher.publish("party.createdOrUpdated", party);
        Broadcaster.broadcast(findAll());
        sendPartyDetails();
    }

    private void savePartyDetails(JSONObject jsonObject) {
        JSONObject partyRecord = jsonObject.getJSONObject("partyDetails");
        Party party = getParty(Long.parseLong(partyRecord.get("id").toString()));
        party.setEnable(Boolean.parseBoolean(partyRecord.get("enabled").toString()));
        save(party);
        //notificationEventPublisher.publish("party.createdOrUpdated", party);
        Broadcaster.broadcast(findAll());
        sendPartyDetails();
    }

    public void sendPartyDetails() {
        JSONObject data = new JSONObject();
        data.put("action", "party.data");
        data.put("partyDetails", getPartyData());
        mqttManager.publish("mobile.subscriber", 2, data.toString(), true);
    }


    public List<Party> findAll() {
        return partyRepository.findAll();
    }


    public Map<String, Object> findByPartyId(Long partId) {
//        return partyRepository.findByPartyId(partId).get(0);
        return new HashMap<>();
    }

    public Party getParty(Long id) {
        return partyRepository.findById(id).get();
    }

    public void save(Party party) {
        partyRepository.save(party);
    }

    public List<Map<String, Object>> getAllParties() {
        List<Party> parties = partyRepository.findAll();
        List<Map<String, Object>> result = new ArrayList<>();
        for (Party party : parties) {
            Map<String, Object> record = new HashMap<>();
            record.put("id", party.getId());
            record.put("name", party.getName());
            record.put("email", party.getEmail());
            record.put("phoneNumber", party.getPhoneNumber());
            record.put("contactPerson", party.getContactPerson());
            record.put("enabled", party.getEnable());
            record.put("street", party.getPhysicalAddress() != null ? party.getPhysicalAddress().getAddress() : "");
            record.put("city", party.getPhysicalAddress() != null ? party.getPhysicalAddress().getCity() : "");
            record.put("zipcode", party.getPhysicalAddress() != null ? party.getPhysicalAddress().getZipCode() : "");
            result.add(record);
        }
        return result;
    }

    public JSONArray getPartyData() {
        List<Map<String, Object>> result = new ArrayList<>();
        List<Map<String, Object>> resultSet = getAllParties();
        for (Map<String, Object> party : resultSet) {
            Map<String, Object> record = new HashMap<>();
            record.put("id", party.get("id"));
            record.put("name", party.get("name"));
            record.put("email", party.get("email"));
            record.put("phoneNumber", party.get("phoneNumber"));
            record.put("isFunder", party.get("isFunder"));
            record.put("isRepresentative", party.get("isRepresentative"));
            record.put("contactPerson", party.get("contactPerson"));
            record.put("enabled", party.get("enabled"));
//            record.put("emailPreferences", party.get("emailPreferences"));


            record.put("street", party.get("street"));
            record.put("city", party.get("city"));
            record.put("zipcode", party.get("zipcode"));

            Map<String, Object> address = new HashMap<>();

            if (party.get("address") != null)
                address.put("address", party.get("address"));


            if (party.get("city") != null)
                address.put("city", party.get("city"));

            if (party.get("state") != null)
                address.put("state", party.get("state"));

            if (party.get("zipCode") != null)
                address.put("zipCode", party.get("zipCode"));
            record.put("address", address);
            result.add(record);
        }
        JSONArray jsonArray = new JSONArray(result);
        return jsonArray;
    }

    public void deleteParty(Party party){
        partyRepository.delete(party);
        //notificationEventPublisher.publish("party.createdOrUpdated", party);
        Broadcaster.broadcast(findAll());
        sendPartyDetails();
    }

    public Long getActivePartyCount(){
        return partyRepository.countByEnable(true);
    }

    public Long getFundersCount(){
        return partyRepository.countByIsFunder(true);
    }

    public Long getPartiesCount(){
        return partyRepository.count();
    }
}
