package com.demo.common.service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.enterprise.context.RequestScoped;
import java.io.File;
import java.io.InputStream;

@Service
@RequestScoped
public class AWSService {

    @Autowired
    private AmazonS3 amazonS3;

    //@Autowired
    //private KeycloakService keycloakService;

    //@Autowired
    //private Environment environment;

    public String getCurrentTenantBucket() {
        String bucket = null;
        //String currentTenant = keycloakService.getCurrentTenant();
//        if (currentTenant != null) {
//            bucket = environment.getProperty("spring.amazonaws.bucket." + currentTenant);
//        }

//        System.out.println("CURRENT TENANT " + currentTenant + " AWS BUCKET: " + bucket);

        return bucket;
    }

    public InputStream getObject(String key) {
        String bucketName = getCurrentTenantBucket();
        S3Object s3Object = amazonS3.getObject(bucketName, key);
        return s3Object.getObjectContent();
    }

    public Boolean doesObjectExist(String key) {
        String bucketName = getCurrentTenantBucket();
        return amazonS3.doesObjectExist(bucketName, key);
    }

    public String putObject(String filePath, File file) {
        String bucketName = getCurrentTenantBucket();
        amazonS3.putObject(bucketName, filePath, file);
        return "https://s3.amazonaws.com/" + bucketName + "/" + filePath;
    }

    public String putObject(String filePath, InputStream inputStream, ObjectMetadata objectMetadata) {
        String bucketName = getCurrentTenantBucket();
        amazonS3.putObject(bucketName, filePath, inputStream, objectMetadata);
        return "https://s3.amazonaws.com/" + bucketName + "/" + filePath;
    }

    public void deleteObject(String key) {
        String bucketName = getCurrentTenantBucket();
        amazonS3.deleteObject(bucketName, key);
    }
}
