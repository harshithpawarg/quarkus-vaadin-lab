package com.demo.common.service;

//import com.bugsnag.Bugsnag;

import com.demo.common.model.User;
import io.quarkus.security.identity.SecurityIdentity;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.AccessToken;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.eclipse.microprofile.jwt.JsonWebToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
//import org.springframework.core.env.Environment;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import java.util.*;

@Service
@RequestScoped
public class KeycloakService {

//    @Autowired
//    Bugsnag bugsnag;

    @Value("${keycloak.realm}")
    private String realm;

    @Value("${keycloak.auth-server-url}")
    private String serverUrl;

    @Value("${keycloak.resource}")
    private String clientId;

    @Value("${keycloak.credentials.secret}")
    private String clientSecret;

    @Value("${spring.keycloak.username}")
    private String adminUser;

    @Value("${spring.keycloak.password}")
    private String adminPassword;

//    @Autowired
//    private Environment environment;

    @Inject
    SecurityIdentity keycloakSecurityContext;

    @Inject
    JsonWebToken accessToken;

    public Map<String, Object> getOtherClaims() {
        Map<String, Object> claims = new HashMap<>();
        for (String claim : accessToken.getClaimNames()) {
            claims.put(claim, accessToken.getClaim(claim));
        }
        return claims;
    }

    public String getCurrentTenant() {
        String tenant = null;
        try {
            //Map<String, Object> claims = accessToken.containsClaim();
            List tenants = (List) accessToken.getClaim("tenant");
            Object currentTenant = accessToken.getClaim("currentTenant");
            System.out.println("tenants: " + tenants + ", currentTenant: " + currentTenant);

            if (tenants.size() == 1 || (currentTenant == null && tenants.size() > 1)) {
                tenant = tenants.get(0).toString();
            } else if (currentTenant != null) {
                tenant = currentTenant.toString();
            } else {
                tenant = TenantContext.getCurrentTenant();
            }
        } catch (Exception e) {
            System.out.println("getCurrentTenant Exception: " + e.getMessage());
            tenant = TenantContext.getCurrentTenant();
        }
        return tenant;
    }

    public String getCurrentUserEmail() {
        try {
            return keycloakSecurityContext.getPrincipal().getName();
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }


    public Map<String, Object> addUser(User user) throws Exception {
        System.out.println("add user to keycloak: " + user.getEmail());
        Keycloak keycloak = keycloakBuilder();
        Map<String, Object> result = new HashMap<>();
        boolean isNewTenantAdded = false;
        String userId = null;


        // Get realm
        RealmResource realmResource = keycloak.realm(realm);
        UsersResource userRessource = realmResource.users();

        List<UserRepresentation> users = userRessource.search(user.getEmail());
        if (users.size() == 0) {
            System.out.println("user does not exist");
            // Define user
            UserRepresentation userRepresentation = new UserRepresentation();
            userRepresentation.setEnabled(true);
            userRepresentation.setUsername(user.getEmail());
            userRepresentation.setFirstName(user.getFirstName());
            userRepresentation.setLastName(user.getLastName());
            userRepresentation.setEmail(user.getEmail());
            Map<String, List<String>> tenants = new HashMap<>();
            List<String> tenantList = new ArrayList<>();
            tenantList.add(getCurrentTenant());
            tenants.put("tenant", tenantList);
            userRepresentation.setAttributes(tenants);
//                userRepresentation.setAttributes(Collections.singletonMap("tenant", Arrays.asList(getCurrentTenant())));

            // Create user (requires manage-users role)
            Response response = userRessource.create(userRepresentation);
            System.out.println("Repsonse: " + response.getStatusInfo());
            String statusInfo = response.getStatusInfo().toString();
            if(statusInfo.equalsIgnoreCase("conflict")) {
                userId = "user exists";
            } else {
                System.out.println(response.getLocation());
                userId = response.getLocation().getPath().replaceAll(".*/([^/]+)$", "$1");
                System.out.println("userId: " + userId + ", " + user.getPassword());
                userRessource = realmResource.users();
                try {
                    CredentialRepresentation passwordCred = new CredentialRepresentation();
                    passwordCred.setTemporary(true);
                    passwordCred.setValue(user.getPassword());
                    passwordCred.setType(CredentialRepresentation.PASSWORD);
                    userRessource.get(userId).resetPassword(passwordCred);
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }

                System.out.printf("User created with userId: %s%n", userId);
            }
        } else {
            System.out.println("User already exists, " + user.getEmail());
            userId = "user exists";
            UserRepresentation existingUser = users.get(0);
            Map<String, List<String>> existingAttributes = existingUser.getAttributes();
            List<String> attributes = existingAttributes.get("tenant");
            String currentTenant = getCurrentTenant();
            if(!attributes.contains(currentTenant)) {
                attributes.add(currentTenant);
                userId = existingUser.getId();
                existingUser.setAttributes(Collections.singletonMap("tenant", attributes));
                userRessource.get(userId).update(existingUser);
                isNewTenantAdded = true;
            }

        }
        result.put("secureUserId", userId);
        result.put("isNewTenantAdded", isNewTenantAdded);
        return result;
    }

    public Boolean deleteUser(String secureUserId) {
        Boolean deleted = false;
        Keycloak keycloak = keycloakBuilder();
        RealmResource realmResource = keycloak.realm(realm);
        UsersResource userRessource = realmResource.users();

        UserRepresentation userRepresentation = userRessource.get(secureUserId).toRepresentation();
        Map<String, List<String>> existingAttributes = userRepresentation.getAttributes();
        String currentTenant = getCurrentTenant();
        System.out.println("secureUserId:" + secureUserId);
        List<String> tenantList = existingAttributes.get("tenant");

        if(secureUserId != null && tenantList.size() == 1) {
            userRessource.delete(secureUserId);
            deleted = true;
            System.out.println("DELETE: successfully deleted user : " + secureUserId);
        } else if(tenantList.contains(currentTenant)) {
            tenantList.remove(currentTenant);
            System.out.println("DELETE: successfully deleted tenant: " + secureUserId);
            deleted = true;
            userRepresentation.setAttributes(Collections.singletonMap("tenant", tenantList));
            userRessource.get(secureUserId).update(userRepresentation);
        }
        return deleted;
    }

    public Boolean updateUser(User user, String password) {
        Boolean updated = false;
        Keycloak keycloak = keycloakBuilder();
        RealmResource realmResource = keycloak.realm(realm);
        UsersResource userRessource = realmResource.users();
        List<UserRepresentation> users = new ArrayList<>();
        UserRepresentation userRepresentation = null;
        String secureUserId = user.getSecureUserId();
        if (secureUserId != null ) {
            System.out.println("secureUserId: " + secureUserId);
            userRepresentation = userRessource.get(secureUserId).toRepresentation();
            System.out.println("UPDATE: User name ===  " + userRepresentation.getUsername());
        }

        if(userRepresentation != null) {
            if (user.getEmail() != null) {
                userRepresentation.setEmail(user.getEmail());
            }
            if (user.getFirstName() != null) {
                userRepresentation.setFirstName(user.getFirstName());
            }
            if (user.getLastName() != null) {
                userRepresentation.setLastName(user.getLastName());
            }

            if (password != null) {
                CredentialRepresentation passwordCred = new CredentialRepresentation();
                passwordCred.setTemporary(false);
                passwordCred.setValue(password);
                passwordCred.setType(CredentialRepresentation.PASSWORD);
                userRessource.get(userRepresentation.getId()).resetPassword(passwordCred);
            }

            System.out.println("Token ID: " + userRepresentation.getId());
            System.out.println("User Email ID: " + userRepresentation.getEmail());
            System.out.println("User Name: " + userRepresentation.getUsername());
            System.out.println("User First Name: " + userRepresentation.getFirstName());
            System.out.println("Last Last Name: " + userRepresentation.getLastName());

            userRessource.get(secureUserId).update(userRepresentation);
            updated = true;
            System.out.println("User updated successfully");
        }

        return updated;

    }

    public boolean updateCurrentTenant(String email, String tenant) {
        boolean updated = false;
        System.out.println("update tenant keycloak => userName:" + email);
        System.out.println("update tenant keycloak => tenant:" + tenant);
        Keycloak keycloak = keycloakBuilder();
        String userId = null;

        // Get realm
        RealmResource realmResource = keycloak.realm(realm);
        UsersResource userRessource = realmResource.users();

//            List<UserRepresentation> users = userRessource.search(email);

        //NOTE: Search user by email only
        List<UserRepresentation> users = userRessource.search(null, null, null, email, null, null);
        System.out.println("User size: " + users.size());

        if (users.size() > 0) {
            UserRepresentation existingUser = users.get(0);
            Map<String, List<String>> existingAttributes = existingUser.getAttributes();
            existingAttributes.put("currentTenant", Arrays.asList(tenant));
            userId = existingUser.getId();
            existingUser.setAttributes(existingAttributes);
            userRessource.get(userId).update(existingUser);
            updated = true;
        } else {
            System.out.println("User " + email + " doesn't exists.");
        }
        return updated;
    }

    public Keycloak keycloakBuilder() {
        Keycloak keycloak = KeycloakBuilder.builder()
                .serverUrl(serverUrl)
                .realm(realm)
                .grantType(OAuth2Constants.PASSWORD)
                .clientId(clientId)
                .clientSecret(clientSecret)
                .username(adminUser)
                .password(adminPassword)
                .build();

        return keycloak;
    }
}
