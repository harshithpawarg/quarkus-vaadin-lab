package com.demo.common.service;

import com.demo.common.model.Tenant;
import com.demo.common.repository.TenantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class TenantManager {

    @Autowired
    TenantRepository tenantRepository;

    public Tenant getTenant(Long tenantId) {
        Tenant tenant = null;
        Optional<Tenant> optionalTenant = tenantRepository.findById(tenantId);
        if(optionalTenant.isPresent())
            tenant = optionalTenant.get();
        return tenant;
    }

    public void saveTenant(Tenant tenant) {
        tenantRepository.save(tenant);
    }
}
