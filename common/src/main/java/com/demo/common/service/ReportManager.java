package com.demo.common.service;

import com.demo.common.model.Report;
import com.demo.common.model.ReportAttribute;
import com.demo.common.repository.ReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
//import org.springframework.transaction.annotation.Transactional;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Component
public class ReportManager {
    @Autowired
    private ReportRepository reportRepository;

    @Autowired
    private ReportAttributeManager reportAttributeManager;

    public Report getReport(Long reportId) {
        return reportRepository.findById(reportId).get();
    }

    public List<Report> findByPermissionContainingIgnoreCase(String role){
        System.out.println("role: " + role);
        return reportRepository.findByPermissionContainingIgnoreCase(role);
    }

    public List<Report> findAll(){
        return reportRepository.findAll();
    }

    public void delete(Report report){
        reportRepository.delete(report);
    }

    public void save(Report report){
        reportRepository.save(report);
    }

    @Transactional
    public void persist(Report report){
        if(report.getId() != null)
        {
            System.out.println("Report Id " + report.getId());
            Long reportID = report.getId();
            Optional<Report> optionalReport = reportRepository.findById(reportID);
            if (optionalReport.isPresent()){
                List<ReportAttribute> reportAttributeByReport = reportAttributeManager.findByReport((optionalReport.get()));
                for (ReportAttribute reportAttribute : reportAttributeByReport) {
                    reportAttributeManager.delete(reportAttribute);
                }

            }
        }
        save(report);
        report.getReportAttributes().forEach(reportAttribute -> {
            reportAttributeManager.save(reportAttribute);
        });
    }
}
