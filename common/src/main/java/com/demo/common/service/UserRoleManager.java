package com.demo.common.service;

import com.demo.common.model.Role;
import com.demo.common.model.User;
import com.demo.common.model.UserRole;
import com.demo.common.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class UserRoleManager {
    @Autowired
    public UserRoleRepository userRoleRepository;

    public void updateUserRole(User user){
        List<Role> roles = user.getRoles();
        roles.forEach(role -> {
            UserRole userRole = userRoleRepository.findByUserIdAndRoleId(user.getId(), role.getId());
           if(userRole == null) userRoleRepository.save(new UserRole(user.getId(), role.getId()));
        });
//        user.setRoles(roles);
    }
}
