package com.demo.common.service;

import com.demo.common.model.Address;
import com.demo.common.repository.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class AddressManager {


    @Autowired
    public AddressRepository addressRepository;
    public Address createAddress(Address address, Map<String, Object> addressBody) {
        address.setAddress(addressBody.get("address") != null ? addressBody.get("address").toString() : null);
        address.setCity(addressBody.get("city") != null ? addressBody.get("city").toString() : null);
        address.setState(addressBody.get("state") != null ? addressBody.get("state").toString() : null);
        address.setZipCode(addressBody.get("zipCode") != null ? addressBody.get("zipCode").toString() : null);
        addressRepository.save(address);
        return address;
    }
}
